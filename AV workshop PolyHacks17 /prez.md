
<!-- <style>
html,body{ margin:0; padding:0; height:100%; width:100%; }

.slide{height:100%;width:100%;overflow:scroll;margin-bottom:2em;}
</style> -->

# Autonomous Vehicle Introduction <small>(with ROS)
### Keely Hill

<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>


## Introduction to autonomous cars programming and our scale model
- Levels of autonomy (0-5)
- Built by us from chassis, sensors, and MIT plans
	- Tegra, running Ubuntu
	- ZED Stereo, Structure
	- LIDAR
	- IMU
	- Motor controller
	- Battery (for computer)
- Game controller as an override.
- Connected via wifi router, ssh to transfer code
- Gazebo simulator
- Challenges
	- Wall following
	- Cone parking using visual data
	- Mapping and local->global commanding
	- Navigate faux city-scape
	- Race with others

<img src="img/chassis2.jpg" style="width:70%;"/>
<br><br>
![](img/RACECAR-front.png)


## ROS

> The Robot Operating System (ROS) is a flexible framework for writing robot software. It is a collection of tools, libraries, and conventions that aim to simplify the task of creating complex and robust robot behavior across a wide variety of robotic platforms.
<br><br>- ros.org


<img src="img/rospubsub.png" style="width:70%;"/>
<br>Note: the Master node is started first


### Example nodes and topics:
- `/scan`
- `/zed/camera_left`
- `/zed/camera_right`
- `/vesc/ackermann_cmd_mux/input/teleop`
- `/vesc/ackermann_cmd_mux/input/navigation`

- `$ rostopic list`

### Messages

Classes and objects in C++/Python


**Ackermann Drive**
```
float32 steering_angle          # desired virtual angle (radians)
float32 steering_angle_velocity # desired rate of change (radians/s)

float32 speed                   # desired forward speed (m/s)
float32 acceleration            # desired acceleration (m/s^2)
float32 jerk                    # desired jerk (m/s^3)
```

**LaserScan**
```
Header header            

float32 angle_min        # start angle of the scan [rad]
float32 angle_max        # end angle of the scan [rad]
float32 angle_increment  # angular distance between measurements [rad]

float32 time_increment   # time between measurements [seconds
float32 scan_time        # time between scans [seconds]

float32 range_min        # minimum range value [m]
float32 range_max        # maximum range value [m]

float32[] ranges         # range data [m] (Note: values < range_min or > range_max should be discarded)
float32[] intensities    # intensity data [device-specific units].
```

**Image**
```
# This message contains an uncompressed image
# (0, 0) is at top-left corner of image

Header header  

uint32 height         # image height, that is, number of rows
uint32 width          # image width, that is, number of columns

string encoding       # Encoding of pixels

uint8 is_bigendian    # is this data bigendian?
uint32 step           # Full row length in bytes
uint8[] data          # actual matrix data, size is (step * rows)
```

**Pose**
```
# A representation of pose in free space, composed of position and orientation.
Point position
Quaternion orientation
```

<br><br>

## ROS Programming

Do one thing, example
```python
import rospy

from ackermann_msgs.msg import AckermannDriveStamped

class GoForwardTurning():
    def __init__(self):

        rospy.init_node('GoForwardTurning', anonymous=False)

        rospy.on_shutdown(self.shutdown)

        # `navigation` is a lower priority than `teleop`, this allows emergency overriding
        self.cmd_vel = rospy.Publisher(
                            '/vesc/ackermann_cmd_mux/input/navigation',
                            AckermannDriveStamped, queue_size=10)

        r = rospy.Rate(10)  # handles 10 times per second

        # change this variable (`move_cmd.drive`) to move and steer
        move_cmd = AckermannDriveStamped()
        move_cmd.drive.speed = 1.5
        move_cmd.drive.steering_angle = 0.5 # radians

        # as long as you haven't ctrl + c keeping doing...
        while not rospy.is_shutdown():

            self.cmd_vel.publish(move_cmd)

            r.sleep()

    def shutdown(self):
        rospy.loginfo("Stoping...")

        self.cmd_vel.publish(AckermannDriveStamped()) # zero all commands

        # sleep just makes sure it receives the stop command prior to shutting down
        rospy.sleep(1)

if __name__ == '__main__':
    try:
        GoForwardTurning()
    except Exception as e:
        rospy.loginfo("GoForwardTurning node terminated.")

```


Adding subscribers:

```python
from sensor_msgs.msg import LaserScan

# ... somewhere in __init__:

# pass: topic, type (class) of message from import, and any callback.
rospy.Subscriber('/scan', LaserScan, self.scan_CB)

# ... somewhere in class body:

def scan_CB(self, scan:LaserScan):
    # get middle range, for example
    scan.ranges[len(scan.ranges)/2]

```


## OpenCV

> OpenCV (Open Source Computer Vision Library) is an open source computer vision and machine learning software library. OpenCV was built to provide a common infrastructure for computer vision applications and to accelerate the use of machine perception in the commercial products. Being a BSD-licensed product, OpenCV makes it easy for businesses to utilize and modify the code.
<br><br>- OpenCV.org

- to greyscale
- cropping
- color thresholding
- filter 'inRange'
- blurring
- Canny Edge detection
- shape finding
- contour identification
- ...much more

<br>
Color masking
```python
def zed_rgb_CB(self, imgmsg):
    img = self.bridge.imgmsg_to_cv2(imgmsg, desired_encoding='bgr8')

    h, w, d = img.shape

    orange_min = np.array([5,50,255])
    orange_max = np.array([15,255,255])

    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    self.cone_mask = cv2.inRange(img_hsv, orange_min, orange_max)

	mask_count = cv2.countNonZero(self.cone_mask)
    if mask_count < 1000:
        self.acker.speed = 0
        self.acker.steering_angle = 0
        return
    elif mask_count > 35000: # 35_000  close, so stop
        self.acker.speed = 0
        return
	# else following color

    M = cv2.moments(self.cone_mask)
    if M['m00'] > 0:

        cx = int(M['m10']/M['m00'])

        err = cx - (w/2)

        self.acker.speed = 1
        self.acker.steering_angle =  -float(err) * 0.001
```

<img src="img/cv-inrange.jpeg"/>


## Mapping, SLAMing, and routing

<img src="img/rosmapex.jpg"/>
<img src="img/firstmap.jpg"/>

- <big>gmapping, Hector mapping
- <big>localization
- <big>global planning
- <big>local planning

## Command Hierarchy
<img src="img/AV-Hier.png"/>
From MIT

## Comparison to full sized cars
- Concepts are the same
- more, better, redundant, sensors
- better computer
- GPS
- teams of people with resources
- more room for mistakes


## Usage of Deep Learning

- Can't program everything
	- All objects and their identification
	- types and variations of road lines
	- signs
	- traffic lights
	- steering
	- semantic meaning

<br><br><br><br><br>
## Questions?
<br><br>
https://gitlab.com/KeelyHill/racecar-codes`
<br/><br/><br/><br/><br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/><br/><br/><br/><br/>
