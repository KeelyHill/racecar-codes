# Various Codes for Rudolph the Dash nosed MIT RACECAR

## See the [course log](/av-course-log.md) for basic programming examples for getting started.

## Directory Descriptions

`AV workshop PolyHacks17` : Keely gave a 30 minute workshop of intro to AVs and out car. Contains the presentation markdown, images, and html.

`basic_stayers` : things that make the car keep a state; stay_middle and cone_ (and their variants).

`board_demo` : lidar and zed color follow demo, swapped by controller buttons.

`gazebo controller` : a ROS node that can control the movement of the car in the gazebo simulator via keyboard input.

`scanCNN` : (WIP) a convolutional neural net trained on lidar scan data and teleop input.

`xinCone` : Xin's attempt at the cone swearing lab.

`rudolph` : A proper ROS package made for the final tests. It has a commander, path planning, and more.
