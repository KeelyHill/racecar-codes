#Alex Vasta
#This is a module for mapping keys, creating functions for the mapped keys, and putting all of these elements together
#to make it easier for the user to define what each mapped key should do.

import Tkinter
import rospy
from ackermann_msgs.msg import AckermannDriveStamped

#Defines the keymap based on readings from the config file reader.
class KeyMap():

    #Constructor
    def __init__(self, master=None):
        self.keyMappingArr = []
        self.keyNamesArr = []
        self.length = len(master.keyMapArr)
        self.i = 0
        self.keyMappingArr.append(self.i)
        self.keyNamesArr.append(self.i)

        # This right here makes me hate my life.
        #This is a for loop that defines the key map and the key names based on the config class readings
        for self.i in range(0, self.length * 2):

            if self.i >= self.length:
                self.keyMappingArr[self.i] = '<KeyRelease-{0}>'.format(master.keyMapArr[self.i - self.length])
                self.keyNamesArr[self.i] = master.keyNameArr[self.i - self.length] + 'Rel'
                self.keyMappingArr.append(self.i)
                self.keyNamesArr.append(self.i)
            else:
                self.keyMappingArr.append(self.i)
                self.keyNamesArr.append(self.i)
                self.keyMappingArr[self.i] = '<KeyPress-{0}>'.format(master.keyMapArr[self.i])
                self.keyNamesArr[self.i] = master.keyNameArr[self.i]



class KeyEventHandler:
    """
    This class is for handling user defined key events.
    It works through saving a dictionary of your commands, because dictionaries are nice and inexpensive.
    to create a key event, first create a key event handler:
    keyEvents = KeyEventHandler

    and then specify the correct functions according to your keybinds.
    one for pressing the key and another for releasing it

    @keyEvents.action
    def on_dash_key(event):
        print('I could clear this sky in 10 seconds flat!')

    THE RELEASE FUNCTION COMMAND IS CURRENTLY BUGGED!!!!
    @keyEvents.action
    def on_dashRel_key(event):
        print('Needs to be about 20% cooler')

    and then you run the key handler after you have specified all of the correct button commands and key mappings
    There is already a rospy node in this class. to add a rospy function to a key, put this inside of the decorator
    rename keyEvents to the name of your defined KeyEventHandler class. An example of how to do this can be found
    in the rkeycommands class.

    while not rospy.is_shutdown():
        keyEvents.pub.publish(keyEvents.drivehandler)
        keyEvents.drivehandler.drive.steering_angle = -.5
        keyEvents.rate.sleep()
        break

    keyHandler = KeyHandler(tkmaster(the tkinter master class),eventmaster(the key event handler master class),bindmaster(the key mapping master class)

    Note: this class already runs a ros publisher and listener within the decorator that you are writing in,
    for your convenience
    :o)
    """

    # Constructor
    def __init__(self, master=None):
        self.actionList = {}
        self.groupList = {}
        self.actionNames = []
        self.actionCount = 0
        self.groupActionList = {}
        self.groupActionNames = []
        self.group = None
        self.prevFunc = None

        # 'rospy stuff... and things'- Rick
        self.pub = rospy.Publisher('/vesc/ackermann_cmd_mux/input/default', AckermannDriveStamped, queue_size=1)
        rospy.init_node('drivehandler')
        self.drivehandler = AckermannDriveStamped()

        #The rate the rospy class publishes
        self.rate = rospy.Rate(100)

    #this is the function that adds defied actions to a specified group that you define in a decorator.
    def action_group(self, group, isTkGroup=False):
        self.group = group

        #If it is not a known group NOTE: THIS FUNCTION IS BROKEN CURRENTLY.
        #IT CURRENTLY JUST USES THE actionlist ARRAY.
        #FIXME: groups
        if not self.group in self.groupList:

            self.group = {group: {'actionList': {}, 'actionNames': []}}
            self.group['actionList'] = {}
            self.group['actionNames'] = []
            self.groupActionList = self.group.get('actionList')
            self.groupActionNames = self.group.get('actionNames')
            self.groupList.update(self.group)
            return self.groupList, self.actionList, self.actionNames

        #if it is a known group
        else:
            self.group = self.groupList.get(self.group, None)
            self.groupActionList = self.group.get('actionList')
            self.groupActionNames = self.group.get('actionNames')
            self.groupList.update(self.group)
        return self.groupList, self.actionList, self.actionNames

    #This is the function that creates and adds decorator defined functions to a specified group
    def action(self, group='default'):

        self.group = self.action_group(group)

        def newaction(func):

            self.actionList[func.__name__] = func
            newaction.all = self.actionList
            return func

        return newaction


# Be sure to run this class AFTER YOU DECLARE EVERY OTHER CLASS.
class KeyHandler(Tkinter.Frame, KeyMap, KeyEventHandler):

    #Constructor
    def __init__(self, tkmaster, eventmaster, bindmaster):
        Tkinter.Frame.__init__(self, tkmaster)
        self.master.config()
        self.master.minsize(width=500, height=500)

        self.currentGroup = 'nope'
        self.actionKeys = eventmaster.actionList.keys()
        self.actions = eventmaster.actionList
        self.keyCommandNames = []
        self.count = 0

        # Checks if the keybind names or hex values are valid.
        # It will not work if the binds are not defined correctly
        for i in range(0, len(bindmaster.keyNamesArr)):
            self.keyCommandNames.append(i)
            self.keyCommandNames[i] = str('on_' + str(bindmaster.keyNamesArr[i]) + '_key')
            self.keyCommandNames[i] = str('on_' + str(bindmaster.keyNamesArr[i]) + '_key')

        for i in range(0, len(self.keyCommandNames)):
            if not self.actions.get(self.keyCommandNames[i]):
                pass

            else:
                try:
                    self.actionToBind = self.actions.get(self.keyCommandNames[i])
                    self.master.bind(bindmaster.keyMappingArr[i], self.actionToBind)
                except Exception:
                    print("ERROR: KEY " + str(bindmaster.keyMappingArr[i])) + "NOT FOUND. THIS KEY IS NOT BOUND."
                    pass



            print("MSG: KEY: " + str(bindmaster.keyMappingArr[i]) + " WAS BINDED TO: " + str(self.actions.get(self.keyCommandNames[i])))