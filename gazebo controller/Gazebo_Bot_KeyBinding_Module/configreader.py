#Alex Vasta
#This is the single function module that populates an array with the proper keys that were defined in the config file


import readerwriter


class Config():
    #Constructors
    def __init__(self, master=None):
        self.fileDict = readerwriter.ReadWriteFile('Configs/', 'KeyBindingConfig', '.config').read_from_file()
        self.keyMapArr = []
        self.keyNameArr = []
        self.skipCount = 0

        #Populates the configArray array with all of the appropriate keys
        for i in range(0, len(self.fileDict)):

            if not '=' in self.fileDict[i]:
                self.skipCount += 1
                pass
            else:
                i -= self.skipCount
                self.keyMapArr.append('')
                self.keyNameArr.append('')
                self.configList = self.fileDict[self.skipCount + i].split()
                self.keyMapArr[i] = self.configList[-1]
                self.keyNameArr[i] = self.configList[0]