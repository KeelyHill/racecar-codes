#Alex Vasta
#This is an example class for how a typical rkeybinds based module should be made and organized.

import rkeybinds
import Tkinter
import configreader
import rospy


#  Define all of the classes needed
#Define the tkinter master
root = Tkinter.Tk()

#Define the config reader master
config = configreader.Config()

#Define the KeyMap master
keyBinds = rkeybinds.KeyMap(config)

#Define the KeyEventHandler master
keyEvents = rkeybinds.KeyEventHandler()

#Define all of the actions the keys should do when they are pressed.
#It will not break the program if you do not define what a mapped key should do
#a rospy module is built into the KeyEventHandler class.
#The key release function is bugged. DO NOT USE.
@keyEvents.action()
def on_left_key(event):
    while not rospy.is_shutdown():
        keyEvents.pub.publish(keyEvents.drivehandler)
        if keyEvents.drivehandler.drive.steering_angle >= .5:
            pass
        else:
            keyEvents.drivehandler.drive.steering_angle += .5
        keyEvents.rate.sleep()
        break
    print('left key pressed')


@keyEvents.action()
def on_right_key(event):
    while not rospy.is_shutdown():
        keyEvents.pub.publish(keyEvents.drivehandler)
        if keyEvents.drivehandler.drive.steering_angle <= -.5:
            pass
        else:
            keyEvents.drivehandler.drive.steering_angle += -.5
        keyEvents.rate.sleep()
        break
    print('right key pressed')


@keyEvents.action()
def on_up_key(event):
    while not rospy.is_shutdown():
        keyEvents.pub.publish(keyEvents.drivehandler)
        keyEvents.drivehandler.drive.speed = 1
        keyEvents.rate.sleep()
        break
    print('up key pressed')


@keyEvents.action()
def on_down_key(event):
    while not rospy.is_shutdown():
        keyEvents.pub.publish(keyEvents.drivehandler)
        keyEvents.drivehandler.drive.speed = -1
        keyEvents.rate.sleep()
        break
    print('down key pressed')


@keyEvents.action()
def on_accelerate_key(event):
    print('acceleration key pressed')


@keyEvents.action()
def on_stop_key(event):
    while not rospy.is_shutdown():
        keyEvents.pub.publish(keyEvents.drivehandler)
        keyEvents.drivehandler.drive.speed = 0
        keyEvents.rate.sleep()
        break
    print('stop key pressed')


@keyEvents.action()
def on_camera_key(event):
    print('camera key pressed')


@keyEvents.action()
def on_auton_key(event):
    print('auton key pressed')


#Define the KeyHandler AFTER YOU DEFINE ALL OF YOUR KEY ACTIONS.
keyHandler = rkeybinds.KeyHandler(root, keyEvents, keyBinds)

#All of the Tkinter stuff that needs to be done.
keyHandler.pack()
keyHandler.focus_set()

#DO NOT FORGET THIS LINE, OTHERWISE YOUR CODE WILL STOP IMMEDIATELY.
keyHandler.mainloop()