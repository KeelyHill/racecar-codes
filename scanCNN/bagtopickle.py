import rosbag
from ackermann_msgs.msg import AckermannDriveStamped
from sensor_msgs.msg import LaserScan
import pickle

teleop_out = {}
scan_out = {}

bag = rosbag.Bag('/media/sdcard/scantele.bag')
for topic, msg, t in bag.read_messages(topics=['/vesc/ackermann_cmd_mux/input/teleop', '/scan']):

    sync_key = str(t.secs) + str(round(t.nsecs/10000000,4))

    if topic == '/vesc/ackermann_cmd_mux/input/teleop':
        teleop_out[sync_key] = [msg.drive.steering_angle,msg.drive.speed]


    if topic == '/scan':
        scan_out[sync_key] = msg.ranges


print len(teleop_out), len(scan_out)

out = {}
for k in teleop_out:
    rs = scan_out.get(k, None)

    if rs:
        out[k] = [teleop_out[k], rs]


print(len(out))

f = open('/media/sdcard/scantele.pickle', 'wb')
pickle.dump(out, f)
f.close()

bag.close()
