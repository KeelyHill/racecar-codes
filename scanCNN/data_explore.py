"""Written to import and get an idea of the data stucture and how to work with it"""

import pickle
import numpy as np

# fix random seed for reproducibility
# seed = 7
# np.random.seed(seed)


def load_data():

    loaded = {}
    with open('scantelefrombag.pickle', 'rb') as f:
        loaded = pickle.load(f)


    X = [] # input, e.g. scan
    Y = []

    for key in loaded:
        steer, speed = loaded[key][0]
        steer = round(steer, 5)
        speed = round(speed, 5)

        # TODO randomly mirror data

        scan = np.array(loaded[key][1])

        X.append(scan)
        Y.append(np.array(steer))

        # organized.append((steer, speed, scan))

    X = np.asarray(X)
    Y = np.asarray(Y)

    half = int(len(X)/2)
    rand_indexes = np.arange(len(X))
    np.random.shuffle(rand_indexes)

    train_X = X[rand_indexes[:half]]
    train_Y = Y[rand_indexes[:half]]

    test_X = X[rand_indexes[half:]]
    test_Y = Y[rand_indexes[half:]]

    return np.array([train_X]), np.array([train_Y]), np.array([test_X]), np.array([test_Y])
