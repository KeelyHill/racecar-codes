from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers import Embedding, Activation
from keras.layers import Conv1D, GlobalAveragePooling1D, MaxPooling1D

import data_explore

def steering_net():
    model = Sequential()

    model.add(Conv1D(1, 3, activation='relu', input_shape=(346, 1081)))
    # model.add(Conv1D(32, 3, activation='relu'))
    # model.add(Activation('relu'))
    # model.add(MaxPooling1D(2))
    #
    # model.add(Conv1D(64, 3, activation='relu'))
    # model.add(Conv1D(64, 3, activation='relu'))
    #
    # model.add(GlobalAveragePooling1D())
    #
    # model.add(Dropout(0.5))
    #
    # model.add(Dense(1, activation='sigmoid'))

    return model

def get_model():
    model = steering_net()
    model.compile(loss = 'mse', optimizer = 'Adam')
    return model


m = get_model()
print(m.summary())

train_X, train_Y, test_X, test_Y = data_explore.load_data()

m.fit(train_X, train_Y.reshape(1,346,1)[:,:344,:], batch_size=16, epochs=5)

score = m.evaluate(test_X, test_Y, batch_size=16)

print("Model score:", score)
