import cmath

class ConePy():
    def __init__(self, velocity, right, left, time, angle):
        self.arrayLidar = [right, left]
        self.velocity = velocity
        self.right = right #right distance of the lidar
        self.left = left #left distance of the lidar
        self.time = time
        self.angle = angle
        self.centri_acce = 0.0
        self.angle_final = 0.0
        self.finalVel = 0.0
        #self.radius = min(self.arrayLidar)
        self.radius = right
        self.setCalculation()



    def printAngle(self):
        print self.velocity
        print self.radius
        print self.time
        print self.angle_final
        print self.centri_acce

    def setCalculation(self):
        self.centri_acce = ((self.velocity * self.velocity) / self.radius)
        self.angle_final = -((3.14159*((self.angle / self.radius) + (self.velocity / self.radius) * self.time + (1/2)*self.centri_acce * self.time**2))/180)
    #    self.finalVel = ((self.velocity *self.radius) + (self.centri_acce *self.time)) / self.radius





#if __name__ == '__main__':
#    test = ConePy(0.1,1,1,0.05,0.8)
#    test.printAngle()
