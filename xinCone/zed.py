import cv2, cv_bridge
import numpy as np
import rospy
from sensor_msgs.msg import Image, PointCloud2


#The ZED Class
class ZED:
    def __init__(self):
        #Declare all of the local variables to use in here
        self.bridge = cv_bridge.CvBridge()
        self.cone_mask = None
        self.cx = 0.0
        self.cy = 0.0
        self.h = 0.0
        self.w = 0.0
        #Subscribes to the ZED Camera
        rospy.Subscriber('/zed/rgb/image_rect_color', Image, self.zed_rgb_CB)
        #rospy.Subscriber('/zed/depth/depth_registered', Image, self.zed_depth_image_CB)
        #rospy.Subscriber('/zed/point_cloud/cloud_registered', PointCloud2, self.zed_depth_cloud_CB)


    def zed_rgb_CB(self, imgmsg):
        #Insert what variables you want it to return here
        img = self.bridge.imgmsg_to_cv2(imgmsg, desired_encoding='bgr8')

        #img = cv2.bilateralFilter(img_norm,9,75,75) # blur, preserve edges

        h, w, d = img.shape
        img[0:200, 0:w] = 0

        orange_min = np.array([5,50,230])
        orange_max = np.array([15,255,255])

        #orange_min = np.array([0,150,45]) # red of staples box
        #orange_max = np.array([16,255,255])

        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

        mask = cv2.inRange(img_hsv, orange_min, orange_max)
        self.cone_mask = mask
        self.centerForImage()

        #print "i am zed"
    def centerForImage(self):  #finding the center of the image

        if (self.cone_mask is not None):
            M = cv2.moments(self.cone_mask)

            if M['m00'] > 0:
                self.h,self.w = self.cone_mask.shape

                self.cx = int(M['m10']/M['m00'])
                self.cy = int(M['m01']/M['m00'])
            else:
                self.cone_mask = None
                print "lost meee....."





    def zed_depth_image_CB(self, data):
        #Insert what variables you want it to return here
        pass
