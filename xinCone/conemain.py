import rospy
from lidar import LIDAR
from zed import ZED
from conePysics import ConePy
from ackermann_msgs.msg import AckermannDriveStamped
from sensor_msgs.msg import Joy

class ConeCommander():
    def __init__(self):
        #test variables


        #self.ZED = ZED()
        self.LIDAR = LIDAR()
        #self.physics = None
        self.ZED = ZED()
        #Initialize the n
        rospy.init_node('Rudolph', anonymous=False)

        rospy.loginfo("To stop: ^C")
        rospy.on_shutdown(self.shutdown)

        # Ackermann drive pub, and zero it
        self.acker_pub = rospy.Publisher('/vesc/ackermann_cmd_mux/input/navigation', AckermannDriveStamped, queue_size=10)

        self.ackermannMsg = AckermannDriveStamped()

        self.acker = self.ackermannMsg.drive
        self.acker.speed = 0
        self.acker.steering_angle = 0.0
        self.rotation = False
        self.direction = -1



        self.is_auto = False
        rospy.Subscriber('/vesc/joy', Joy, self.joy_CB)


    #The joystick method
    def joy_CB(self, joy):
        if joy.buttons[0] == 1: # A
            self.is_auto = True

        elif joy.buttons[2] == 1: # X
            self.acker.speed = 0
            self.publish_acker()
            self.is_auto = False

    #The ackermann publishing method
    def publish_acker(self):
        if self.is_auto:
            self.acker_pub.publish(self.ackermannMsg)
        else:
            self.acker_pub.publish(AckermannDriveStamped())



    def speedUp(self, value):
        self.acker.speed = self.acker.speed + value


    def slowDown(self, value):
        if self.acker.speed > 0:
            self.acker.speed = self.acker.speed - value

    def stop(self, value):
        for i in range(value):
            self.acker.speed = 0.0
            self.acker_pub.publish(self.ackermannMsg)

    def reverse(self, angle, value):
        self.acker.steering_angle = angle



    def zedConeDetect(self, lidarValue, start):
        rotateStart = False
        if ((start - lidarValue) > 1.0):
            rotateStart = True
        return rotateStart


#The start function
    def start(self, rate=15):
        r = rospy.Rate(rate)
        if (self.ZED.cone_mask is not None):

            print "Cone detected with center at: ", self.ZED.cx, ", ", self.ZED.cy
            #if self.ZED.cx > self.ZED.w/2: #located to the right side of the frame turn left and start there
                #self.direction = -self.direction
            startRad = self.LIDAR.right
            self.acker.steering_angle = 0
            self.acker.speed = 1
            startFront = self.LIDAR.front


            while not rospy.is_shutdown():
                if self.ZED.cone_mask is not None:
                    print self.ZED.cx, "frame size is: ", self.ZED.w
                if self.direction == -1:
                    print "direction is right.....", self.rotation
                    if (self.rotation == False and self.zedConeDetect(self.LIDAR.right, startRad) == True):
                        self.rotation = True
                        startFront = self.LIDAR.front
                        self.acker.steering_angle = -0.5
                        self.ZED.cone_mask = None
                    if  self.rotation == True and (self.ZED.cx < self.ZED.w/2):
                        startRad = self.LIDAR.left
                        self.direction = -self.direction
                        self.rotation = False

                if self.direction == 1:
                    print "direction is left....."
                    if (self.rotation == False and self.zedConeDetect(self.LIDAR.left, startRad) == True):
                        self.rotation = True
                        startFront = self.LIDAR.front
                        self.acker.steering_angle = 0.5
                    if  self.rotation == True and (self.ZED.cx > self.ZED.w/2) :
                        startRad = self.LIDAR.right
                        self.direction = -self.direction
                        self.rotation = False


                self.publish_acker()

                r.sleep()
        else:
            print "no cone initially detected....."




    def straightCones(self): # test this with ZED enabled
        coneTotal = 5
        coneNum = 0
        rate = 20
        r = rospy.Rate(rate)
        self.acker.speed = 4
        self.acker.steering_angle = 0.0
        startRad = 0.0
        if (self.ZED.cone_mask is not None):
            if self.ZED.cx > self.ZED.w/2: #depends on the initial cone location
                startRad = self.LIDAR.right
                self.direction = -1
            elif self.ZED.cx < self.ZED.w/2:
                startRad = self.LIDAR.left
                self.direction = 1
            print "Found Cone, Direction set to: ", self.direction
        while coneNum < coneTotal - 1 and not rospy.is_shutdown():
            if self.direction == -1:
                if (self.zedConeDetect(self.LIDAR.right, startRad) == True):
                    coneNum = coneNum + 1
                    startRad = self.LIDAR.right
                    if self.LIDAR.right - startRad > 1:
                        startRad = self.LIDAR.right
            elif self.direction == 1:
                print "not here..."
                if (self.zedConeDetect(self.LIDAR.left, startRad) == True):
                    coneNum = coneNum + 1
                    startRad = self.LIDAR.left
            self.publish_acker()
            r.sleep()
            print "number of cone passed: ", coneNum
        while not rospy.is_shutdown():
            print "looking for turning point..."
            self.circle(startRad)
            self.publish_acker()
            r.sleep()


    def circle(self, startRad):
        if self.direction == -1:
            print "direction is right....."
            if (self.rotation == False and self.zedConeDetect(self.LIDAR.right, startRad) == True):
                self.rotation = True
                self.acker.steering_angle = -0.5
            #startRad = self.LIDAR.right
            if self.ZED.cx < self.ZED.w/2:
                print "coneDetected, switching to left focus"
                startRad = self.LIDAR.left
                self.direction = -self.direction
                self.rotation = False


                #self.tick(startRad)
        if self.direction == 1:
            print "direction is left....."
            if (self.rotation == False and self.zedConeDetect(self.LIDAR.left, startRad) == True):
                self.rotation = True
                self.acker.steering_angle = 0.5
            #startRad = self.LIDAR.left
            if  self.ZED.cx > self.ZED.w/2:
                print "coneDetected, switching to right focus"
                startRad = self.LIDAR.right
                self.direction = -self.direction
                self.rotation = False






    def shutdown(self):
        rospy.loginfo("Stopping Rudolph")
        #cv2.destroyAllWindows()

        # sleep makses sure topics recive their 'stop' message fore shutting down bot
        rospy.sleep(1)


if __name__ == '__main__':
    c = ConeCommander()
    c.start()
    #c.straightCones()
