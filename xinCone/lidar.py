import cv2
from sensor_msgs.msg import LaserScan
import numpy as np
import rospy
import math


#The LIDAR class, that will return all of the LIDAR data
class LIDAR:
    def __init__(self):

        #Subscribe to the LIDAR
        rospy.Subscriber('/scan', LaserScan, self.scan_CB)

        #Declare all of the local variables to be used by the master class
        self.scanObj = None
        self.scans_len = 0.0
        self.scans_len = 0.0
        self.right = 0.0
        self.left = 0.0
        self.front = 0.0
        self.frontClear = True



    #Grabs all of the sensor's data
    def scan_CB(self, scan):
        self.scanObj = scan
        self.scans_len = len(scan.ranges)
        self.right = scan.ranges[180]
        self.left = scan.ranges[900]
        self.front = scan.ranges[540]


    def obs(self, index, max):
        if index < 180 or index > 900:
            return
        if self.frontClear == False:
            return
        if self.scanObj.ranges[index] < max:
            self.frontClear = False
            print "index blocked is: ", index
            return
        self.obs(index + 1, max*math.cos(0.25/180*3.14))
        self.obs(index - 1, max*math.cos(0.25/180*3.14))
    
