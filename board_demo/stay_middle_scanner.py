
import numpy as np

def determine_acker(scan):
    scans_len = len(scan.ranges)
    right = scan.ranges[scans_len/3]
    left = scan.ranges[2*scans_len/3]

    middle_view_min = np.average(scan.ranges[2*scans_len/5 : 3*scans_len/5])

    err = round(right-left, 2) / 2

    # print(left, right, middle_view_min, err)

    if middle_view_min < 1.5: # turn away from wall
        steer_direction = -1 if right-left > 0 else 1
        steering_angle = 0.6 * steer_direction
        speed = 1.5

        #print('there is a wall in front of me')

    else: # stay in middle of walls

        steering_angle = -0.25 * err
        speed = 5

        #print('normal staying')

    return (steering_angle, speed)
