import cv2
import cv_bridge
import numpy as np


bridge = cv_bridge.CvBridge()

# keep passing data for 'video'
def imgshow(image, named='Demo_view'):
    cv2.namedWindow(named, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(named, 960, 540)

    cv2.imshow(named, image)
    cv2.waitKey(1) #needed for some reason, 1 mili sec, shows image and keeps it updating


class COLORS: # in HSV
    orange_min = np.array([5,50,255])
    orange_max = np.array([15,255,255])

    red_min = np.array([0,170,45]) # red of staples box
    red_max = np.array([8,255,255])

    green_min = np.array([40,80,30]) # TODO
    green_max = np.array([80,255,255])

def determine_acker(imgmsg):
    img = bridge.imgmsg_to_cv2(imgmsg, desired_encoding='bgr8')

    h, w, d = img.shape
    # img[0:200, 0:w] = 0 # cut off top

    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    red_mask = cv2.inRange(img_hsv, COLORS.red_min, COLORS.red_max)

    # imgshow(red_mask)

    if cv2.countNonZero(red_mask) > 4500:
        print('sees stop color')
        return (0, 0, True) # see red

    green_mask = cv2.inRange(img_hsv, COLORS.green_min, COLORS.green_max)

    #imgshow(green_mask)

    if cv2.countNonZero(red_mask) < 5000 and cv2.countNonZero(green_mask) < 5000:
        #print('cant see any color to follow')
        return (0,0, False)


    # sees enough green at this point to continue

    M = cv2.moments(green_mask)

    if M['m00'] > 0:
        h,w = green_mask.shape

        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])
        # cv2.circle(image, (cx, cy), 20, (0,0,255), -1)

        # find error, and steer
        err = cx - (w/2 + 75) # 75 is an offset for right camera

        if cv2.countNonZero(green_mask) > 270000: # 250_000  close, so stop
            speed = 0
        else:
            speed = 1

        steering_angle =  -float(err) * 0.002 # need a better func/eq for this

        return (steering_angle, speed, False)
