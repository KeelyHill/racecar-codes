import rospy
from ackermann_msgs.msg import AckermannDriveStamped
from sensor_msgs.msg import Image, LaserScan, Joy

import cv2

import stay_middle_scanner
import color_follower

STATE_MAN = 0
STATE_STAY_MIDDLE = 1
STATE_FOLLOW_GREEN = 2 # red will override both

class Demo(object):

    acker = None

    def __init__(self):
        rospy.init_node('BoardDemp', anonymous=False)

        rospy.loginfo("To stop: ^C")
        rospy.on_shutdown(self.shutdown)

        # Ackermann drive pub, and zero it
        self.acker_pub = rospy.Publisher('/vesc/ackermann_cmd_mux/input/navigation', AckermannDriveStamped, queue_size=10)

        self.ackermannMsg = AckermannDriveStamped()

        self.acker = self.ackermannMsg.drive
        self.acker.speed = 0
        self.acker.steering_angle = 0

        ## Subscribers

        rospy.Subscriber('/scan', LaserScan, self.scan_CB)

        rospy.Subscriber('/zed/rgb/image_rect_color', Image, self.zed_rgb_CB)

        # commanding variables

        self.see_stop_color = False

        self.auto_state = 0
        rospy.Subscriber('/vesc/joy', Joy, self.joy_CB)


    def scan_CB(self, scan):

        if self.see_stop_color:
            self.acker.steering_angle = 0
            self.acker.speed = 0
            return

        if self.auto_state == STATE_STAY_MIDDLE:

            angle, speed = stay_middle_scanner.determine_acker(scan)
            self.acker.steering_angle = angle
            self.acker.speed = speed



    def zed_rgb_CB(self, data):

        angle, speed, sees_red = color_follower.determine_acker(data)

        self.see_stop_color = sees_red

        if self.auto_state == STATE_FOLLOW_GREEN: # will also stop with red
            self.acker.steering_angle = angle
            self.acker.speed = speed


    def joy_CB(self, joy):
        if joy.buttons[0] == 1: # A
            self.auto_state = STATE_FOLLOW_GREEN
        elif joy.buttons[3] == 1: # Y
            self.auto_state = STATE_STAY_MIDDLE
        elif joy.buttons[2] == 1: # X
            self.auto_state = STATE_MAN
            self.acker.speed = 0
            self.publish_acker()



    def publish_acker(self):
        #print('state:', self.auto_state)

        if self.auto_state == STATE_STAY_MIDDLE or self.auto_state == STATE_FOLLOW_GREEN:
            self.acker_pub.publish(self.ackermannMsg)
        else:
            self.acker_pub.publish(AckermannDriveStamped())

    # def tick(self):
    #     pass

    def start(self, rate=30):
        r = rospy.Rate(rate)

        while not rospy.is_shutdown():

            # self.tick()
            self.publish_acker()

            r.sleep()

    def shutdown(self):
        rospy.loginfo("Stopping Board Demo")
        cv2.destroyAllWindows()

        # sleep makses sure topics recive their 'stop' message fore shutting down bot
        rospy.sleep(1)





if __name__ == '__main__':
    try:
        d = Demo()
        d.start()
    except Exception as e:
        print("Exception: "+ str(e))
