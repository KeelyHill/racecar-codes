#Alex Vasta
#This is the module for reading and writing to files.


import os

#File reader and writer class for all of your file reading and writing needs
class ReadWriteFile:
    #Constructor
    def __init__(self, fileLoc, fileName, fileExtension):

        #location of the file
        if os.path.exists(fileLoc):
            self.fileLoc = fileLoc
        if not os.path.isfile:
            print('ERROR: INVALID FILE LOCATION. SETTING FILE LOCATION TO NONE')
            self.fileLoc = ''

        #File name variables
        self.fileName = fileName
        self.fileExtension = fileExtension
        self.theFile = fileLoc + fileName + fileExtension
        self.fileDict = {}
        self.createNewFile = False


    #Creates a new file
    def create_new_file(self):
        global counter
        counter = 0
        if self.createNewFile == True:

            if os.path.isfile(self.theFile) is True:

                print('WARNING: File ' + self.theFile + ' already exists!')

                while os.path.isfile(self.theFile):
                    counter += 1
                    newFileName = self.fileName + str(counter)
                    self.theFile = self.fileLoc + newFileName + self.fileExtension

            if not os.path.isfile(self.theFile):
                print('MSG: Creating new file')

                with open(self.theFile, 'a+', encoding='ascii') as file:
                        file.write('')
                        file.close()

        elif self.createNewFile == False:
            print('WARNING: Not creating new file, because user specified that a new file should not be created.')
            return


    #Reads everything from the file and puts it into an array named fileArray.
    def read_from_file (self):

        if os.path.isfile(self.theFile) is True:
            print('MSG: File ' + self.theFile + ' exists! reading it to an array')

            with open(self.theFile, 'r') as file:
                self.fileDict = file.readlines()
                file.close()
            return self.fileDict

        if not os.path.isfile(self.theFile):

            if self.createNewFile == True:
                self.create_new_file()

            elif self.createNewFile == False:
                return


    #Appends to a file
    def append_to_file(self,text):
        if os.path.isfile(self.theFile) is True:
            print('MSG: file ' + self.theFile + ' exists! appending to the next line in the file!')

            with open(self.theFile, 'a+') as file:
                file.write('\n')
                file.write(text)
                file.close()
        if not os.path.isfile(self.theFile):

            if self.createNewFile == True:
                self.create_new_file()

            elif self.createNewFile == False:
                return


    #Checks if the file in the class exists and returns true or false.
    def fileExists(self):
        if os.path.isfile(self.theFile) is True:
            return True

        if not os.path.isfile(self.theFile) is False:
            return False