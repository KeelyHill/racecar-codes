
import subprocess
import vlc
import time
import readerwriter
import os


"""Small class for playing and handling sounds with a special names.
Very platform specific

Usage:
>>> sound = Sounder()
>>> sound.play('ready')

No. Just, no. I've go something better for ya.
go do 'pip install python-vlc' and 'sudo apt-get install vlc'
then run this.
vlc is better than subprocessing.
you can also mix around the file types with vlc.
:3
"""

#Totally not a modified class from my gazebo thing.
class SoundEffects():

    # Constructor
    def __init__(self, master=None):
        self.fileDict = readerwriter.ReadWriteFile('Configs/', 'SoundConfig', '.config').read_from_file()
        self.soundNameArr = []
        self.skipCount = 0
        self.configList = []
        self.configs = [1,0]
        self.instance = vlc.Instance()
        self.media = ''

        #These values are how high the volume is by a percent.
        self.SOUND_VOLUME = 100
        self.MUSIC_VOLUME = 85

        #sseconds/sminutes = sound seconds/minutes, and mseconds/mminutes = music seconds/minutes.
        #basically, the duration of each item.
        self.sMinutes = 0
        self.sSeconds = 0
        self.mMinutes = 0
        self.mSeconds = 0
        self.soundPlayer = self.instance.media_player_new()
        self.musicPlayer = self.instance.media_player_new()

        # Default directory where all of the sounds are located.
        self.sDir = 'Sounds/'

        # EUROBEAT, BOIS! This is what all the mlg pro racers listen to.
        #The default sound it reverts to if it does not detect a sound.
        self.defaultSound = 'sandroll.mp3'

        # Populates the configArray array with all of the appropriate directory names.
        # Go change the song through the SoundConfig file in the configs folder. Also, add sounds to the Sounds folder.
        #Remove all spaces from sound titles, or the program will fail
        for i in range(0, len(self.fileDict)):

            if not '=' in self.fileDict[i]:
                self.skipCount += 1
                pass

            else:
                i -= self.skipCount
                self.soundNameArr.append(i)
                self.configList = self.fileDict[self.skipCount + i].split()
                self.configs[0] = self.configList[0]
                self.configs[1] = self.configList[2]
                self.soundNameArr[i] = self.configs[1]


        #This is a bunch of try and except loops, to see if they are proper files
        for i in range(0,len(self.soundNameArr)):
            if os.path.isfile(self.sDir + self.soundNameArr[i]):
                print('MSG: Song ' + self.soundNameArr[i] + ' exists!')
                pass

            if not os.path.isfile(self.sDir + self.soundNameArr[i]):
                print('ERROR: ' + str(self.soundNameArr[i]) + ' FILE NOT FOUND. SETTING IT TO THE DEFAULT SOUND')
                self.soundNameArr[i] = self.defaultSound


        self.readyS = self.instance.media_new(self.sDir + self.soundNameArr[0])
        self.autoStartS = self.instance.media_new(self.sDir + self.soundNameArr[1])
        self.autoStopS = self.instance.media_new(self.sDir + self.soundNameArr[2])
        self.deadAheadS = self.instance.media_new(self.sDir + self.soundNameArr[3])
        self.ambientMusicS = self.instance.media_new(self.sDir + self.soundNameArr[4])


    #Plays sounds
    def play_sound(self, sound):
        self.soundPlayer.set_media(sound)
        self.soundPlayer.play()
        self.soundPlayer.audio_set_volume(self.SOUND_VOLUME)
        time.sleep(.1)  # I do not like sleep methods, but it's the only way to get the length of the song
        duration = self.soundPlayer.get_length() / 1000
        self.sMinutes, self.sSeconds = divmod(duration, 60)
        print 'MSG: Sound ' + sound.get_mrl() + ' Is now playing. Total Length of the sound file:', "%02d:%02d" % (self.sMinutes, self.sSeconds)


    #Plays a music track on a seperate handler then the sound handler
    def play_music(self, sound):
        self.musicPlayer.set_media(sound)
        self.musicPlayer.play()
        self.musicPlayer.audio_set_volume(self.MUSIC_VOLUME)
        time.sleep(.1)
        duration = self.musicPlayer.get_length() / 1000
        self.mMinutes, self.mSeconds = divmod(duration, 60)
        print 'MSG: Song ' + sound.get_mrl() + ' Is now playing. Total Length of the song file:', "%02d:%02d" % (self.mMinutes, self.mSeconds)


    #Stops sounds
    def stop_sound(self):
        self.soundPlayer.stop()
        print 'MSG: The sound has been stopped!'


    #Stops the music
    def stop_music(self):
        self.musicPlayer.stop()
        print 'MSG: The song has been stopped!'


'''
class Sounder:

    name_to_file = {
        'ready': 'ready-for-action.wav',
        'auto-start': 'here-i-go.wav',
        'auto-stop': 'you-take-over.wav',
        'dead-ahead': 'something-dead-ahead.wav'
    }

    something_ahead_played = False

    def play(self, name):

        if name in self.name_to_file:

            if name == 'dead-ahead' and self.something_ahead_played:
                return
            elif name == 'dead-ahead':
                self.something_ahead_played = True

            filename = self.name_to_file[name]
            print('TODO do play: %s' % filename)
            subprocess.call(['afplay', '/Users/Keely/Desktop/Keely Voice/' + filename])
            # subprocess.call(['aplay', '~/kvoice/' + filename])
        else:
            print('No sound named: %s' % name)
'''

if __name__ == '__main__':
    i = 0

    sounds = SoundEffects()
    sounds.play_music(sounds.autoStartS)

    #I wanted the song to play all the way through, so i did a quik algorithim
    #Feel free to get rid of it, or look at it.
    seconds = sounds.mSeconds + (sounds.mMinutes * 60)
    secondsRemaining = seconds
    sounds.play_sound(sounds.ambientMusicS)
    time.sleep(6)

    while secondsRemaining >= 0:
        i += 1
        sounds.play_sound(sounds.autoStopS)
        time.sleep(1)
        secondsRemaining = seconds - i
        print('Seconds Remaining: ' + str(secondsRemaining))

    sounds.play_music(sounds.readyS)


    i = 0
    seconds = sounds.mSeconds + (sounds.mMinutes * 60)
    secondsRemaining = seconds
    sounds.play_sound(sounds.deadAheadS)

    while secondsRemaining != 0:
        i += 1
        sounds.play_sound(sounds.deadAheadS)
        time.sleep(1)
        secondsRemaining = seconds - i
        print('Seconds Remaining: ' + str(secondsRemaining))

    sounds.stop_sound()


    time.sleep(seconds)
    state = sounds.musicPlayer.get_state()

    #Just a side note, all sound files will run until the program or sound ends. So, do not forget to use the stop
    #methods!
