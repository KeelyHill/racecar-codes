#!/usr/bin/env python

import rospy

import cv2
import numpy as np

from std_msgs.msg import UInt8

"""

Publishes UInt8 to `/rudolph/shape_detect` on state change
 0: no shape
 1: stop sign (octagon)
 2: rectangle (could be square)
 3: triangle
"""
class ZEDShapeLooker():
    def __init__(self):
        rospy.init_node('shape_looker', anonymous=False)

        rospy.on_shutdown(self.shutdown)

        self.ZEDRightCam = None
        self.ZEDLeftCam = None
        self.frame = None
        self.width = None
        # self.shape = "?shape?"

        self.last_shape_int_published = -1
        self.shape_pub = rospy.Publisher('/rudolph/shape_detect', UInt8, queue_size=5)

        #Loop through until it finds the proper fram width for the ZED
        for i in range (0,5):
            self.ZEDFeed = cv2.VideoCapture(i)

            ret, self.frame = self.ZEDFeed.read()

            try:
                #The self.frame is the frame, the length of self.frame is the height, and the length of any array inside of self.frame is the width.
                #Yes, that is how opencv works
                self.width = len(self.frame[0])
            except:
                self.width = 0.0

            # check zed, The frame width should be larger then 1280
            if self.width > 600 and ret:
                self.ZED_CAMERA_PORT = i

                print('ZED Height:' + str(len(self.frame)))
                print('ZED Width:' + str(len(self.frame[0])))

                rospy.loginfo("Found ZED camera stream.")
                return

            #If not then continue on through the loop

        # If got here, ZED not found (i think)
        print('ZED not found!!!!')


    temp_shape_int = 0
    times_been_same = 0
    TIMES_SAME_OKAY = 3
    def tick(self):
        shapes = self.get_shapes()

        shape_as_int = 0
        if shapes:
            shapes.sort(key = lambda x: x[1], reverse=True)
            biggest_shape = shapes[0][0] # first index, then key
            shape_as_int = {'oct':1, 'pent':2, 'tri':3}.get(biggest_shape, 0)


        # temp_shape_int used to smooth out flickers
        if shape_as_int != self.temp_shape_int:
            self.temp_shape_int = shape_as_int
            self.times_been_same = 0
        else:
            self.times_been_same += 1


        if self.last_shape_int_published != shape_as_int and self.times_been_same > self.TIMES_SAME_OKAY:
            # print('biggest shape pub int=%i' % shape_as_int)
            self.times_been_same = self.TIMES_SAME_OKAY + 1

            self.last_shape_int_published = shape_as_int
            int_msg = UInt8()
            int_msg.data = shape_as_int
            self.shape_pub.publish(int_msg)


    def start(self, rate=10):
        rospy.loginfo("Starting ZED shape looker")
        r = rospy.Rate(rate)
        while not rospy.is_shutdown():
            self.tick()
            r.sleep()


    def get_shapes(self):

        # red_lower = np.array([0,20,100])
        # red_upper = np.array([15,255,255])

        lower = np.array([42,100,20])
        upper = np.array([100,255,255])

        right_frame = self.get_right_frame()
        # right_frame = cv2.resize(right_frame, (0,0), fx=0.8, fy=0.8)

        hsv = cv2.cvtColor(right_frame, cv2.COLOR_BGR2HSV)

        mask = cv2.inRange(hsv, lower, upper)
        mask = cv2.dilate(mask, None, iterations=1)

        # red_mask = cv2.inRange(hsv, red_lower, red_upper)
        # red_mask = cv2.dilate(red_mask, None, iterations=1)

        # self.imgshow(mask)

        cnts = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

        shapes = [] # of tuples (shape abrv string, radius)

        ## Green mask contours for direction indicating shapes
        for c in cnts:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.04 * peri, True) # Approximates a polygonal curve(s) with the specified precision.

            ((x, y), radius) = cv2.minEnclosingCircle(c)

            if radius > 25:

                num_edge_name_map = {3:'tri', 5:'pent', 8:'oct'}

                shape = num_edge_name_map.get(len(approx), None)

                if shape is None:
                    shape = 'some other shape'

                shapes.append((shape, radius))

        # ## Red mask contours for stop sign
        # cnts = cv2.findContours(red_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        # for c in cnts:
        #     peri = cv2.arcLength(c, True)
        #     approx = cv2.approxPolyDP(c, 0.07 * peri, True) # Approximates a polygonal curve(s) with the specified precision.
        #
        #     ((x, y), radius) = cv2.minEnclosingCircle(c)
        #
        #     if radius > 40 and len(approx) == 8:
        #         shapes.append(('oct', radius))

        # print(shapes)
        return shapes



    def get_right_frame(self):
        ret, self.frame = self.ZEDFeed.read()
        return self.frame[:len(self.frame), 0:int(len(self.frame[0])/2)]

    #This function will split the video feed into 2 video feeds. one for the left side and one for the right side.
    def split_zed_images(self):

        self.rect, self.frame = self.ZEDFeed.read()
        #Sets the left and right camera feeds
        self.ZEDRightCam= self.frame[:len(self.frame), 0:int(len(self.frame[0])/2)]
        self.ZEDLeftCam = self.frame[:len(self.frame), int(len(self.frame[0])/2):len(self.frame[0])]\

        #Displays the length of each of the sides
        # print('Left Side Height:' + str(len(self.ZEDLeftCam)))
        # print('Left Side Width:' + str(len(self.ZEDLeftCam[0])) + '\n')
        # print('Right Side Height:' + str(len(self.ZEDRightCam)))
        # print('Right Side Width:' + str(len(self.ZEDRightCam[0])) + '\n')

        #Shows the video feed
        # cv2.imshow('leftframe', self.ZEDLeftCam)
        # cv2.imshow('rightframe', self.ZEDRightCam)



    def imgshow(self, image, named='shape_looker_view'):
        cv2.namedWindow(named, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(named, 960, 540)

        cv2.imshow(named, image)
        cv2.waitKey(1) #needed for some reason, 1 mili sec, shows image and keeps it updating

    def shutdown(self):
        self.ZEDFeed.release()


if __name__ == '__main__':
    try:
        zsl = ZEDShapeLooker()
        zsl.start()

    except rospy.ROSInterruptException:
        pass


# #This is the old (Alex's) main function for this python file
# if __name__ == '__main__':
#     ZEDc = ZED()
#     shape = []
#
#     while True:
#         ZEDc.detect_object(edges = 4)
#         print ("The shape detected was a " + str(ZEDc.shape))
#
#         if cv2.waitKey(1) & 0xFF == ord('q'):
#             break
#     print('ending program')
#     #ZEDc.out.release()
#     ZEDc.ZEDFeed.release()
#     cv2.destroyAllWindows()
