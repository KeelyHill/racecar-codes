#!/usr/bin/env python

# Author: christoph.roesmann@tu-dortmund.de

import rospy, math
from geometry_msgs.msg import Twist
from ackermann_msgs.msg import AckermannDriveStamped


def convert_trans_rot_vel_to_steering_angle(v, omega, wheelbase):
    if omega == 0 or v == 0:
        return 0

    radius = v / omega
    return math.atan(wheelbase / radius)


last_acker = None

def cmd_callback(data):
    global wheelbase
    global ackermann_cmd_topic
    global frame_id
    global pub
    global last_acker

    v = data.linear.x

    if 0.35 > v > 0.0001: # scale it to add needed motor torqe
        v = 0.35
    if -0.35 < v < -0.0001:
        v = -0.35

    steering = convert_trans_rot_vel_to_steering_angle(v, data.angular.z, wheelbase)

    msg = AckermannDriveStamped()
    # msg.header.stamp = rospy.Time.now()
    # msg.header.frame_id = frame_id
    msg.drive.steering_angle = steering
    msg.drive.speed = v

    last_acker = msg

    pub.publish(msg)


if __name__ == '__main__':

    try:
        rospy.init_node('cmd_vel_to_ackermann_drive')

        twist_cmd_topic = rospy.get_param('~twist_cmd_topic', '/cmd_vel')
        ackermann_cmd_topic = '/vesc/ackermann_cmd_mux/input/navigation'
        wheelbase = rospy.get_param('~wheelbase', 0.3)
        frame_id = rospy.get_param('~frame_id', '/vesc/odom')

        rospy.Subscriber(twist_cmd_topic, Twist, cmd_callback, queue_size=10)
        pub = rospy.Publisher(ackermann_cmd_topic, AckermannDriveStamped, queue_size=10)

        rospy.loginfo("Node 'cmd_vel_to_ackermann_drive' started.\nListening to %s, publishing to %s. Frame id: %s, wheelbase: %f", "/cmd_vel", ackermann_cmd_topic, frame_id, wheelbase)

        r = rospy.Rate(3)
        while not rospy.is_shutdown():

            if last_acker:
                pub.publish(last_acker)

            print(last_acker)

            r.sleep()

        rospy.spin()


    except rospy.ROSInterruptException:
        print('cmd_vel to ackermann stopped due to a ROSInterruptException')
        pass
