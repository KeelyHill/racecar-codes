#!/usr/bin/env python

import rospy

from ackermann_msgs.msg import AckermannDriveStamped
from geometry_msgs.msg import Twist, PoseStamped, Pose2D, PoseWithCovarianceStamped
from cmd_vel_to_ackermann_drive import convert_trans_rot_vel_to_steering_angle
from tf.transformations import quaternion_from_euler, euler_from_quaternion

from sensor_msgs.msg import LaserScan, Joy
from std_msgs.msg import UInt8
# from nav_msgs.msg  import Odometry

import numpy as np

def make2dPose(x,y,theta=None):
    p = Pose2D()
    p.x = x
    p.y = y
    p.theta = theta
    return p

class NextGoal:
    """Each argument should be a Pose"""
    def __init__(self, dest, next_goal, left=None, right=None):
        self.dest = dest  # current destination goal
        self.next = next_goal
        self.left = left  # next to turn left, if applicable
        self.right = right  # next to turn right, if applicable


class RudolphCommand(object):

    acker = None

    def __init__(self):
        rospy.init_node('Rudolph_teb', anonymous=False)

        rospy.loginfo("To stop: ^C")
        rospy.on_shutdown(self.shutdown)

        # sensor holdings:

        self.lidar_front_min = 99
        self.waiting_at_stop_sign = False

        # Ackermann drive pub, and zero it
        self.acker_pub = rospy.Publisher('/vesc/ackermann_cmd_mux/input/navigation', AckermannDriveStamped, queue_size=10)

        self.ackermannMsg = AckermannDriveStamped()

        self.acker = self.ackermannMsg.drive
        self.acker.speed = 0
        self.acker.steering_angle = 0

        self.teb_acker = AckermannDriveStamped().drive  # teb recommendation
        self.teb_acker.speed = 0
        self.teb_acker.steering_angle = 0

        self.wheelbase = rospy.get_param('~wheelbase', 0.34)

        self.next_shape_action_int = 0
        self.goals = [
            NextGoal(make2dPose(1.5, 6.5), make2dPose(-3.5,6.7)),
            NextGoal(make2dPose(-3.5,6.7), make2dPose(-4,5)),
            NextGoal(make2dPose(-4,5), make2dPose(1,4)),
            NextGoal(make2dPose(1,4), make2dPose(1.5, 6.5)),

            # # Intersections:
            # NextGoal(make2dPose(0,0), make2dPose(0,0), left=make2dPose(0,0), right=make2dPose(0,0)),
            # NextGoal(make2dPose(0,0), make2dPose(0,0), left=make2dPose(0,0), right=make2dPose(0,0)),
            #
            # NextGoal(make2dPose(0,0), make2dPose(0,0), left=make2dPose(0,0), right=make2dPose(0,0)),
            # NextGoal(make2dPose(0,0), make2dPose(0,0), left=make2dPose(0,0), right=make2dPose(0,0)),
            #
            # # Loop waypoints
            # NextGoal(make2dPose(0,0), make2dPose(0,0)),
            # NextGoal(make2dPose(0,0), make2dPose(0,0)),
            #
            # NextGoal(make2dPose(0,0), make2dPose(0,0)),
            # NextGoal(make2dPose(0,0), make2dPose(0,0)),


        ]
        self.reached_goal_index = -1


        ## Other publishers

        self.set_goal_publisher = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=10)


        ## Subscribers

        rospy.Subscriber('/scan', LaserScan, self.scan_CB)

        twist_cmd_topic = rospy.get_param('~twist_cmd_topic', '/cmd_vel')
        rospy.Subscriber(twist_cmd_topic, Twist, self.teb_twist_CB)

        rospy.Subscriber('/amcl_pose', PoseWithCovarianceStamped, self.localized_pose_CB)

        rospy.Subscriber('/rudolph/shape_detect', UInt8, self.shape_detect_CB)

        self.is_auto = False
        rospy.Subscriber('/vesc/joy', Joy, self.joy_CB)



    def scan_CB(self, scan):
        scans_len = len(scan.ranges)
        self.lidar_front_min = min(scan.ranges[2*scans_len/5 : 3*scans_len/5])


    def localized_pose_CB(self, pose_stamped): # PoseWithCovarianceStamped
        self.last_pose_stamped = pose_stamped

        pose = pose_stamped.pose.pose

        q = pose.orientation
        q_theta = euler_from_quaternion((q.x,q.y,q.z,q.w))[2] # yaw
        myPose = make2dPose(pose.position.x, pose.position.y, q_theta)

        for i, goal in enumerate(self.goals):
            if self.is_between_bounds(myPose.x, goal.dest.x, 0.5) and \
                   self.is_between_bounds(myPose.y, goal.dest.y, 0.5) and \
                   self.reached_goal_index != i:

                self.reached_goal_index = i  # avoid being at same goal twice in a row

                goal_to_publish = None

                if self.next_shape_action_int == 2 and goal.left: # pentagon
                    goal_to_publish = goal.left
                elif self.next_shape_action_int == 3 and goal.right: # triangle
                    goal_to_publish = goal.right
                else: # 0 (no sign) or no left/right goal to set to
                    goal_to_publish = goal.next


                goal_stamped = PoseStamped()
                goal_stamped.header.frame_id = 'map'
                goal_stamped.pose.position.x = goal_to_publish.x
                goal_stamped.pose.position.y = goal_to_publish.y

                goal_theta = goal_to_publish.theta if goal_to_publish.theta else 0
                quaternion = quaternion_from_euler(0,0,goal_theta)
                goal_stamped.pose.orientation.x = quaternion[0]
                goal_stamped.pose.orientation.y = quaternion[1]
                goal_stamped.pose.orientation.z = quaternion[2]
                goal_stamped.pose.orientation.w = quaternion[3]

                print('auto set new goal:')
                print(goal_to_publish)

                self.set_goal_publisher.publish(goal_stamped)

                break




    def shape_detect_CB(self, int_msg):
        shape_int = int_msg.data

        if shape_int == 1: #stop sign
            self.waiting_at_stop_sign = True
            rospy.Timer(rospy.Duration(2), self.go_after_stop, oneshot=True)
        else:
            print('Next shape action: %i' % shape_int)
            self.next_shape_action_int = shape_int

    """Callback used to stop waiting at stop sign"""
    def go_after_stop(self, event):
        self.waiting_at_stop_sign = False
        print('Now going through stop sign')


    """Acts as a MUX deciding to go with TEB's drive or not
    Looks at:
    - lidar too close too fast
    - (TODO) stop from camera choice
    """
    def tick(self):

        if self.lidar_front_min < 0.4 and self.teb_acker.speed > 2: # TODO fiddle with these
            self.acker = AckermannDriveStamped().drive
            print('!* Lidar detects object too close while going fast')
            return

        if self.waiting_at_stop_sign:
            self.acker = AckermannDriveStamped().drive
            print("Waiting at stop sign...")
            return


        # default if no return
        self.acker.speed = self.teb_acker.speed
        self.acker.steering_angle = self.teb_acker.steering_angle

    """TEB's Twist message for keeping on path"""
    def teb_twist_CB(self, twist):
        v = twist.linear.x

        if 0.58 > v > 0.0001: # scale it to add needed motor torque
            v = 0.58
        if -0.58 < v < -0.0001:
            v = -0.58

        steering = convert_trans_rot_vel_to_steering_angle(v, twist.angular.z, self.wheelbase)

        self.teb_acker.speed = v
        self.teb_acker.steering_angle = steering


    def publish_acker(self):
        if self.is_auto:
            self.acker_pub.publish(self.ackermannMsg)
        else:
            self.acker_pub.publish(AckermannDriveStamped())


    def joy_CB(self, joy):
        if joy.buttons[0] == 1: # A
            self.is_auto = True
        elif joy.buttons[2] == 1: # X
            self.acker.speed = 0
            self.publish_acker()
            self.is_auto = False


    def is_between_bounds(self, tester, goal, bound):
        return (goal-bound) < tester < (goal+bound)


    def start(self, rate=20):
        r = rospy.Rate(rate)

        while not rospy.is_shutdown():

            self.tick()
            self.publish_acker()

            r.sleep()

    def shutdown(self):
        rospy.loginfo("Stopping Rudolph")

        # sleep makses sure topics recive their 'stop' message fore shutting down bot
        rospy.sleep(1)



if __name__ == '__main__':
    try:
        r = RudolphCommand()
        r.start()
    except rospy.ROSInterruptException:
        pass
