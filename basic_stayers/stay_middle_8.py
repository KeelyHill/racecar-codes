"""stay_middle.py
Uses two points on the forward sides of the LIDAR to get the cars position
in a hallway. It then steers to center itself.
"""

import rospy
from Rudolph import Rudolph
import cv2
import numpy as np
from std_msgs.msg import UInt8

class StayMiddle(Rudolph):

    def __init__(self):
        Rudolph.__init__(self)
        self.acker.speed = 0

        self.backing_up = False
        self.waiting_at_stop_sign = False
        rospy.Subscriber('/rudolph/shape_detect', UInt8, self.shape_detect_CB)


    def scan_CB(self, scan):

        scans_len = len(scan.ranges)
        right = scan.ranges[scans_len/3]
        left = scan.ranges[2*scans_len/3]

        middle_view_min = min(scan.ranges[2*scans_len/5 : 3*scans_len/5])

        err = round(right-left, 2) / 2

        # print(left, right, middle_view_min, err)

        if self.waiting_at_stop_sign:
            self.acker.speed = 0
            print('waiting stop sign')
            return

        if self.backing_up:
            self.acker.speed = -0.7
            # self.acker.steering_angle *= -1
            self.acker.steering_angle = 0.4 * err
            print('backing up')
            return

        if middle_view_min < 0.45:
            self.acker.speed = 0

            if not self.backing_up:
                self.backing_up = True
                rospy.Timer(rospy.Duration(0.35), self.end_backup, oneshot=True)


        elif middle_view_min < .75: # turn away from wall
            steer_direction = -1 if right-left > 0 else 1
            self.acker.steering_angle = 0.6 * steer_direction
            self.acker.speed = 0.7
            print('there is a wall in front of me')

        else: # stay in middle of walls

            self.acker.steering_angle = -0.2 * err
            self.acker.speed = 1.0

            print('normal staying')


    def shape_detect_CB(self, int_msg):
        shape_int = int_msg.data

        if shape_int == 2: #stop sign
            self.waiting_at_stop_sign = True
            rospy.Timer(rospy.Duration(2), self.go_after_stop, oneshot=True)
        # else:
        #     print('Next shape action: %i' % shape_int)
        #     self.next_shape_action_int = shape_int


    def go_after_stop(self, event):
        self.waiting_at_stop_sign = False


    def end_backup(self, event):
        self.backing_up = False


    #     self.waiting_at_stop_sign = False
    #     self.next_shape_action_int = 0
    #
    #     # rospy.Subscriber('/rudolph/shape_detect', UInt8, self.shape_detect_CB)
    #
    #
    # def shape_detect_CB(self, int_msg):
    #     shape_int = int_msg.data
    #
    #     if shape_int == 1: #stop sign
    #         self.waiting_at_stop_sign = True
    #         rospy.Timer(rospy.Duration(2), self.go_after_stop, oneshot=True)
    #     else:
    #         print('Next shape action: %i' % shape_int)
    #         self.next_shape_action_int = shape_int
    #
    #
    # """Callback used to stop waiting at stop sign"""
    # def go_after_stop(self, event):
    #     self.waiting_at_stop_sign = False
    #     print('Now going through stop sign')


if __name__ == '__main__':
    try:
        sm = StayMiddle()
        sm.start()
    except Exception as e:
        print("Exception: "+ str(e))
