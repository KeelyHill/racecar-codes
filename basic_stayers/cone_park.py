from Rudolph import Rudolph
import cv2, cv_bridge
import numpy as np

import struct

class Cone(Rudolph):

    cone_mask = None # openCV image mask

    def __init__(self):
        Rudolph.__init__(self)
        self.acker.speed = 0

        self.bridge = cv_bridge.CvBridge()


    def tick(self):

        if (self.cone_mask is None):
            return


        mask_count = cv2.countNonZero(self.cone_mask)
        if mask_count < 1000:
            print('cant see anything to follow')
            self.acker.speed = 0
            self.acker.steering_angle = 0
            return
        elif mask_count > 35000: # 250_000  close, so stop
            print('cone close, so stopping')
            self.acker.speed = 0
            # self.acker.steering_angle = 0
            return
        else:
            print('following color')


        M = cv2.moments(self.cone_mask)

        if M['m00'] > 0:
            h,w = self.cone_mask.shape

            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])

            # find error, and steer
            err = cx - (w/2 + 0) # 75 is an offset for right camera

            self.acker.speed = 1
            self.acker.steering_angle =  -float(err) * 0.001 # need a better func/eq for this

           # print(err)


    def zed_rgb_CB(self, imgmsg):

        img_norm = self.bridge.imgmsg_to_cv2(imgmsg, desired_encoding='bgr8')

        img = img_norm
        #img = cv2.bilateralFilter(img_norm,9,75,75) # blur, preserve edges

        h, w, d = img.shape
        img[0:200, 0:w] = 0

        orange_min = np.array([5,50,255])
        orange_max = np.array([15,255,255])

        orange_min = np.array([0,150,45]) # red of staples box
        orange_max = np.array([16,255,255])

        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

        mask = cv2.inRange(img_hsv, orange_min, orange_max)
        self.cone_mask = mask


        #params = cv2.SimpleBlobDetector_Params()
        #params.blobColor = 255
        #params.filterByColor = True
        #params.minArea = 2
        #params.filterByArea = True
        #detector = cv2.SimpleBlobDetector_create(params)
        #keypoints = detector.detect(255-mask)


       # self.imgshow(mask, 'mask')
       # self.imgshow(img, 'rgb')



    def scan_CB(self, scan):
        pass

    def zed_depth_image_CB(self, imgmsg):

        dmap = np.fromstring(imgmsg.data, dtype='<f4')

        dmap[dmap >= 7 ] = 0
        dmap[dmap < 7 ] = 255
        dmap[np.isnan(dmap)] = 0


        dmap.shape = (imgmsg.height, imgmsg.width)

        # print(cv2.countNonZero(dmap))
        # print(dmap)
        # self.imgshow(dmap)




if __name__ == '__main__':
    try:
        c = Cone()
        c.start(20) # rate
    except Exception as e:
        print("Exception: "+ str(e))
