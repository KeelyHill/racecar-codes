import rospy

from math import atan, pi

import std_msgs.msg
from ackermann_msgs.msg import AckermannDriveStamped
from sensor_msgs.msg import Image, LaserScan, Joy
from nav_msgs.msg  import Odometry, Path
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped, Pose2D, PointStamped

from tf.transformations import quaternion_from_euler, euler_from_quaternion


def make2dPoseStamped(x,y): # where x is forward and y is side to side
    ps = PoseStamped()
    ps.pose.x = x
    ps.pose.y = y
    return ps

def make2dPose(x,y,theta=None):
    p = Pose2D()
    p.x = x
    p.y = y
    p.theta = theta
    return p

class FollowPath:

    acker = None

    def __init__(self):
        rospy.init_node('MapPathFollower', anonymous=False)

        rospy.loginfo("To stop: ^C")
        rospy.on_shutdown(self.shutdown)

        # Ackermann drive pub, and zero it
        self.acker_pub = rospy.Publisher('/vesc/ackermann_cmd_mux/input/navigation', AckermannDriveStamped, queue_size=10)
        self.steer_err_pub = rospy.Publisher('/rudolph/steer_error', PoseStamped, queue_size=10)

        self.ackermannMsg = AckermannDriveStamped()

        self.acker = self.ackermannMsg.drive
        self.acker.speed = 0
        self.acker.steering_angle = 0

        # self.path = Path()
        self.path = [make2dPose(10.06,-8,13), make2dPose(11.79, -1.68), make2dPose(12,2.3)]

        self.cur_goal = self.path.pop(0)

        ## Subscribers

        # rospy.Subscriber('/scan', LaserScan, self.scan_CB)
        # rospy.Subscriber('/vesc/odom', Odometry, self.odom_CB)
        rospy.Subscriber('/amcl_pose', PoseWithCovarianceStamped, self.localized_pose_CB)
        rospy.Subscriber('/clicked_point', PointStamped, self.clicked_point_CB)

        self.is_auto = False
        rospy.Subscriber('/vesc/joy', Joy, self.joy_CB)

        self.last_pose_stamped = None


    def tick(self):

        if self.last_pose_stamped not None:
            self.do_P_controler_tick()

    def do_P_controler_tick(self):
        pose = self.last_pose_stamped.pose.pose

        q = pose.orientation
        q_theta = euler_from_quaternion((q.x,q.y,q.z,q.w))[2] # yaw

        myPose = make2dPose(pose.position.x, pose.position.y, q_theta)

        if self.cur_goal == None:
            print('NO MORE GOALS LEFT!!!!!')
            self.acker.steering_angle = 0
            self.acker.speed = -0.2
            return

        dx = self.cur_goal.x - myPose.x
        dy = self.cur_goal.y - myPose.y

        goal_theta = atan(dy/dx) # angle from cur pos to goal
        goal_err_theta = goal_theta - myPose.theta

        print(pose.position)
        print("goal_theta=%f  my orientation=%f  goal_error=%f" % (goal_theta, myPose.theta, goal_err_theta))
        print('cur_goal:')
        print(self.cur_goal)

        self.acker.steering_angle = goal_err_theta * 0.3
        self.acker.speed = 0.7

        goal_reached = self.check_goal_reached(myPose)

        print('goal reached=', goal_reached)



        # for display in rviz

        err_pose = PoseStamped()

        err_pose.header = std_msgs.msg.Header()
        err_pose.header = pose_stamped.header
        err_pose.header.stamp = rospy.Time.now()

        err_pose.pose.position = pose.position

        q_error = quaternion_from_euler(0,0,goal_theta)
        err_pose.pose.orientation.x = q_error[0]
        err_pose.pose.orientation.y = q_error[1]
        err_pose.pose.orientation.z = q_error[2]
        err_pose.pose.orientation.w = q_error[3]

        self.steer_err_pub.publish(err_pose)

        print('\n')

    def is_between_bounds(self, tester, goal, bound):
        return (goal-bound) < tester < (goal+bound)

    """Checks if goal is reached (with bounds), updates goal if so."""
    def check_goal_reached(self, myPose): # -> Bool

        if self.is_between_bounds(myPose.x, self.cur_goal.x, 0.5) and \
               self.is_between_bounds(myPose.y, self.cur_goal.y, 0.5):

            if len(self.cur_goal) > 0:
                self.cur_goal = self.path.pop(0)
            else:
                self.cur_goal = None

            return True

        return False

    def localized_pose_CB(self, pose_stamped): # PoseWithCovarianceStamped
        self.last_pose_stamped = pose_stamped


    def clicked_point_CB(self, point_stamped):
        point = point_stamped.point
        self.cur_goal = make2dPose(point.x, point.y)
        print("New goal set to rviz clicked point: %f, %f" % (point.x, point.y))




    def scan_CB(self, scan):
        pass

    def odom_CB(self, odom):
        pass

    def joy_CB(self, joy):
        if joy.buttons[0] == 1: # A
            self.is_auto = True
        elif joy.buttons[2] == 1: # X
            self.acker.speed = 0
            self.publish_acker()
            self.is_auto = False


    def publish_acker(self):
        if self.is_auto:
            self.acker_pub.publish(self.ackermannMsg)
        else:
            self.acker_pub.publish(AckermannDriveStamped())


    def start(self, rate=20):
        r = rospy.Rate(rate)

        while not rospy.is_shutdown():

            self.tick()
            self.publish_acker()

            r.sleep()

    def shutdown(self):
        rospy.loginfo("Stopping Rudolph")

        # sleep makses sure topics recive their 'stop' message fore shutting down bot
        rospy.sleep(1)



if __name__ == '__main__':
    try:
        fp = FollowPath()
        fp.start(20) # rate
    except Exception as e:
        print("Exception: "+ str(e))
