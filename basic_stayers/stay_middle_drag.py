"""stay_middle_drag.py
Same as the normal stay_middle, but will stop when there is something in front of it.
"""

from Rudolph import Rudolph
import cv2
import numpy as np

class StayMiddle(Rudolph):

    def __init__(self):
        Rudolph.__init__(self)
        self.acker.speed = 0


    def scan_CB(self, scan):

        scans_len = len(scan.ranges)
        right = scan.ranges[scans_len/3]
        left = scan.ranges[2*scans_len/3]

        middle_view_min = min(scan.ranges[2*scans_len/5 : 3*scans_len/5])

        # frac = 10  # for easy selection of each side of middle range
        # middle_view_min = min(scan.ranges[ (scans_len/2)-(scans_len/frac) : (scans_len/2)+(scans_len/frac) ])

        err = round(right-left, 2) / 2

	    print(left, right, middle_view_min, err)

        if middle_view_min < 1.75: # stop at wall
            self.acker.steering_angle = 0
            self.acker.speed = 0.0
            print('there is a wall in front of me, stopping')

        else: # stay in middle of walls

            self.acker.steering_angle = -0.2 * err
            self.acker.speed = 10

            print('normal staying')





if __name__ == '__main__':
    try:
        sm = StayMiddle()
        sm.start()
    except Exception as e:
        print("Exception: "+ str(e))
