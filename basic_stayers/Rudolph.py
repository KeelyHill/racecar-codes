import rospy
from ackermann_msgs.msg import AckermannDriveStamped
from sensor_msgs.msg import Image, LaserScan, Joy, PointCloud2
from nav_msgs.msg  import Odometry

import cv2
import numpy as np

class Rudolph(object):

    acker = None

    def __init__(self):
        rospy.init_node('Rudolph', anonymous=False)

        rospy.loginfo("To stop: ^C")
        rospy.on_shutdown(self.shutdown)

        # Ackermann drive pub, and zero it
        self.acker_pub = rospy.Publisher('/vesc/ackermann_cmd_mux/input/navigation', AckermannDriveStamped, queue_size=10)

        self.ackermannMsg = AckermannDriveStamped()

        self.acker = self.ackermannMsg.drive
        self.acker.speed = 0
        self.acker.steering_angle = 0

        ## Subscribers

        rospy.Subscriber('/scan', LaserScan, self.scan_CB)

        rospy.Subscriber('/zed/rgb/image_rect_color', Image, self.zed_rgb_CB)
        rospy.Subscriber('/zed/depth/depth_registered', Image, self.zed_depth_image_CB)
        rospy.Subscriber('/zed/point_cloud/cloud_registered', PointCloud2, self.zed_depth_cloud_CB)

        rospy.Subscriber('/vesc/odom', Odometry, self.odom_CB)

        self.is_auto = False
        rospy.Subscriber('/vesc/joy', Joy, self.joy_CB)


    def scan_CB(self, scan):
        pass

    def zed_rgb_CB(self, data):
        pass

    def zed_depth_image_CB(self, data):
        pass

    def zed_depth_cloud_CB(self, data):
        pass

    def odom_CB(self, odom):
        pass

    def joy_CB(self, joy):
        if joy.buttons[0] == 1: # A
            self.is_auto = True
        elif joy.buttons[2] == 1: # X
            self.acker.speed = 0
            self.publish_acker()
            self.is_auto = False

    # keep passing data for 'video'
    def imgshow(self, image, named='Rudolph_view'):
        cv2.namedWindow(named, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(named, 960, 540)

        cv2.imshow(named, image)
        cv2.waitKey(1) #needed for some reason, 1 mili sec, shows image and keeps it updating

    def publish_acker(self):
        if self.is_auto:
            self.acker_pub.publish(self.ackermannMsg)
        else:
            self.acker_pub.publish(AckermannDriveStamped())

    def tick(self):
        pass

    def start(self, rate=20):
        r = rospy.Rate(rate)

        while not rospy.is_shutdown():

            self.tick()
            self.publish_acker()

            r.sleep()

    def shutdown(self):
        rospy.loginfo("Stopping Rudolph")
        cv2.destroyAllWindows()

        # sleep makses sure topics recive their 'stop' message fore shutting down bot
        rospy.sleep(1)
