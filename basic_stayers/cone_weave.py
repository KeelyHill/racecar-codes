from Rudolph import Rudolph
import cv2, cv_bridge
import numpy as np

import struct

class CSState:
    sl = ['Looking', 'Go Left', 'Go Right', 'Left Clear', 'Right Clear', 'Left Next', 'Right Next']
    LOOKING, GO_LEFT, GO_RIGHT, LEFT_CLEAR, RIGHT_CLEAR, LEFT_NEXT, RIGHT_NEXT = range(7)

    @staticmethod
    def isClearing(e):
        return e == CSState.LEFT_CLEAR or e == CSState.RIGHT_CLEAR

    @staticmethod
    def toText(e):
        return CSState.sl[e]

class Cone(Rudolph):

    cone_mask = None # openCV image mask

    def __init__(self):
        Rudolph.__init__(self)
        self.acker.speed = 0

        self.bridge = cv_bridge.CvBridge()

        self.cone_site_state = CSState.GO_LEFT
        self.clearing_count = 0

        self.lidar_right_min = 999
        self.lidar_left_min = 999

        # used in pole bending test for
        self.pole_bending_total = 0
        self.cone_pass_count = 0

    def tick(self):

        print('cone site state: %s' % CSState.toText(self.cone_site_state))

        new_speed = 0.75
        new_steer = 0

        if CSState.isClearing(self.cone_site_state):
            self.clearing_count += 1

        # if 20 * 1.7 >= self.clearing_count >= 20 * 0.7: # 1 seconds   may replace this code with a lidar based radius steering angel
        #     print('clearing steering ')
        #     if self.cone_site_state == CSState.LEFT_CLEAR:
        #         new_steer = -0.5
        #     elif self.cone_site_state == CSState.RIGHT_CLEAR:
        #         new_steer = 0.5

        if self.clearing_count >= 20 * 2: # 2 seconds
            # self.clearing_count = 0
            #
            # if self.cone_site_state == CSState.LEFT_CLEAR:
            #     self.cone_site_state = CSState.GO_RIGHT
            # elif self.cone_site_state == CSState.RIGHT_CLEAR:
            #     self.cone_site_state = CSState.GO_LEFT
            pass

        # print('lidar right min', self.lidar_right_min)

        # use lidar to track if cone has passed
        if (self.cone_site_state == CSState.LEFT_CLEAR or self.cone_site_state == CSState.GO_LEFT) and self.lidar_right_min < 1:
            self.cone_site_state = CSState.RIGHT_NEXT
            # self.cone_site_state = CSState.GO_RIGHT

        elif (self.cone_site_state == CSState.RIGHT_CLEAR or self.cone_site_state == CSState.GO_RIGHT)  and self.lidar_left_min < 1:
            self.cone_site_state = CSState.LEFT_NEXT
            # self.cone_site_state = CSState.GO_LEFT


        if self.cone_site_state == CSState.RIGHT_NEXT or self.cone_site_state == CSState.GO_RIGHT:
            new_steer = -0.30
        elif self.cone_site_state == CSState.LEFT_NEXT or self.cone_site_state == CSState.GO_LEFT:
            new_steer = 0.30

        if (self.cone_mask is not None):

            if cv2.countNonZero(self.cone_mask) < 1000: # dont see cone color
                print('dont see cone')
                # if self.cone_site_state == CSState.GO_LEFT:
                #     self.cone_site_state = CSState.LEFT_CLEAR
                #     self.clearing_count = 0
                # elif self.cone_site_state == CSState.GO_RIGHT:
                #     self.cone_site_state = CSState.RIGHT_CLEAR
                #     self.clearing_count = 0


                new_speed = 0.75
                # new_steer = 0
            else:
                print('following color')

                if self.cone_site_state == CSState.RIGHT_NEXT:
                    self.cone_site_state = CSState.GO_RIGHT
                elif self.cone_site_state == CSState.LEFT_NEXT:
                    self.cone_site_state = CSState.GO_LEFT

                M = cv2.moments(self.cone_mask)
                err = 0

                if M['m00'] > 0:
                    h,w = self.cone_mask.shape

                    cx = int(M['m10']/M['m00'])
                    cy = int(M['m01']/M['m00'])
                    # cv2.circle(image, (cx, cy), 20, (0,0,255), -1)

                    if self.cone_site_state == CSState.GO_LEFT:
                       err = cx - (w + 75)  # 75 is an offset for right camera
                    elif self.cone_site_state == CSState.GO_RIGHT:
                        err = cx - (75)
                else:
                    pass
                    # if self.cone_site_state == CSState.GO_LEFT:
                    #    err = -100
                    # elif self.cone_site_state == CSState.GO_RIGHT:
                    #     err = 100

                new_speed = 0.75
                new_steer =  -float(err) * 0.0005 # need a better func/eq for this

                # print('err =', -err)
        else:
            print('image is NONE!!!')

        self.acker.speed = new_speed
        self.acker.steering_angle = new_steer


    def zed_rgb_CB(self, imgmsg):

        img = self.bridge.imgmsg_to_cv2(imgmsg, desired_encoding='bgr8')

        h, w, d = img.shape
        img[0:200, 0:w] = 0

        orange_min = np.array([5,50,225])
        orange_max = np.array([18,255,255])

        orange_min = np.array([0,150,45]) # red of staples box
        orange_max = np.array([16,255,255])

        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

        mask = cv2.inRange(img_hsv, orange_min, orange_max)
        self.cone_mask = mask


        #params = cv2.SimpleBlobDetector_Params()
        #params.blobColor = 255
        #params.filterByColor = True
        #params.minArea = 2
        #params.filterByArea = True
        #detector = cv2.SimpleBlobDetector_create(params)
        #keypoints = detector.detect(255-mask)


       # self.imgshow(mask, 'mask')
       # self.imgshow(img, 'rgb')



    def scan_CB(self, scan):

        self.lidar_right_min = min(scan.ranges[170:190])  # 45 * 4 = 180
        self.lidar_left_min = min(scan.ranges[890:910]) # 1080 - (45*4) = 900



    def zed_depth_image_CB(self, imgmsg):
        return

        # dmap = np.fromstring(imgmsg.data, dtype='<f4')
        #
        # dmap[dmap >= 7 ] = 0
        # dmap[dmap < 7 ] = 255
        # dmap[np.isnan(dmap)] = 0
        #
        #
        # dmap.shape = (imgmsg.height, imgmsg.width)
        #
        # # print(cv2.countNonZero(dmap))
        # print(dmap)

        # self.imgshow(dmap)




if __name__ == '__main__':
    try:
        c = Cone()
        c.start(20) # rate
    except Exception as e:
        print("Exception: "+ str(e))
