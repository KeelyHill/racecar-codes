import cv2
import numpy as np
import rospy
import time
import imutils
from sensor_msgs.msg import Image


#The ZED Class
class ZED:
    def __init__(self):
        #ZED startup functions
        self.ZEDRightCam = None
        self.ZEDLeftCam = None
        self.rect = None
        self.frame = None
        self.left = 0
        self.right = 0
        self.width = None
        self.gray = None 
        self.shape = "Mewtwo"
        self.approx = None


        #Loop through until it finds the proper fram width for the ZED
        for i in range (0,5):
                self.ZEDFeed = cv2.VideoCapture(i)

                self.rect, self.frame = self.ZEDFeed.read()
                
                try:
                    #The self.frame is the frame, the length of self.frame is the height, and the length of any array inside of self.frame is the width.
                    #Yes, that is how opencv works
                    self.width = len(self.frame[0])
                except:
                    self.width = 0.0

                #The frame width should be larger then 1280
                if self.width > 600:
                    self.ZED_CAMERA_PORT = i

                    #Create the video saving stuff
                    #Currently commented out, but it will work if you uncomment out everything that uses this
                    #self.fourcc = cv2.VideoWriter_fourcc('X','V','I','D')
                    #self.out = cv2.VideoWriter('output.avi', self.fourcc , 20, (len(self.frame), len(self.frame[0])))
                    print('Height:' + str(len(self.frame)))
                    print('Width:' + str(len(self.frame[0])))
                    return

                #If not then continue on through the loop
                else:
                    pass


    #Gets the ZED camera feed
    def get_zed_feed(self):
        self.rect, self.frame = self.ZEDFeed.read()
        #self.out.write(self.frame)
        #cv2.imwrite('picture.jpg', self.frame)

        # Display the resulting frame
        cv2.imshow('frame',self.frame)



    #This function will split the video feed into 2 video feeds. one for the left side and one for the right side.
    def split_zed_images(self):

        self.rect, self.frame = self.ZEDFeed.read()
        #Sets the left and right camera feeds
        self.ZEDRightCam= self.frame[:len(self.frame), 0:int(len(self.frame[0])/2)]
        self.ZEDLeftCam = self.frame[:len(self.frame), int(len(self.frame[0])/2):len(self.frame[0])]

        #Displays the length of each of the sides
        print('Left Side Height:' + str(len(self.ZEDLeftCam)))
        print('Left Side Width:' + str(len(self.ZEDLeftCam[0])) + '\n')
        print('Right Side Height:' + str(len(self.ZEDRightCam)))
        print('Right Side Width:' + str(len(self.ZEDRightCam[0])) + '\n')

        #Writes a video to the ZED
        #self.out.write(self.ZEDLeftCam)

        #Shows the video feed
        cv2.imshow('leftframe', self.ZEDLeftCam)
        cv2.imshow('rightframe', self.ZEDRightCam)

    #This function will search for an object with a specific shape and color, and return a
    #array the shape is in on the ZED.
    def detect_object(self,lower = [29, 86, 6], upper = [64, 255, 255], edges=4, rgb = True):
        self.rect, self.frame = self.ZEDFeed.read()
        self.shape = "NULL"
        #self.origFrame = self.frame
        self.frame = imutils.resize(self.frame, width=600)
        self.lower = np.array(lower)
        self.upper = np.array(upper)
        #self.ratio = self.origFrame.shape[0] / float(self.frame.shape[0])

        self.hsv = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)

        self.mask = cv2.inRange(self.hsv, self.lower, self.upper)
        self.mask = cv2.erode(self.mask, None, iterations=2)
        self.mask = cv2.dilate(self.mask, None, iterations=2)

        self.cnts = cv2.findContours(self.mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        self.center = None
        i = 0

        for c in self.cnts:
            self.c = c
            self.peri = cv2.arcLength(self.c, True)
            self.approx = cv2.approxPolyDP(self.c, 0.038 * self.peri, True)
            ((self.x, self.y), self.radius) = cv2.minEnclosingCircle(self.c)
            if self.radius > 10:
                self.M = cv2.moments(self.c)
                self.x = (self.M["m10"] / self.M["m00"])
                self.y = (self.M["m01"] / self.M["m00"])
                print('object found. amount of edges: ' + str(len(self.approx)))

                '''
                if len(self.approx) > 10 + edges:
                    print (str(len(self.approx)) + " Is a greater range then expected.")
                    for i in range (0,len(self.approx)):
                        #try:
                            #self.c[i] =(self.c[i] + self.c[i+2] + self.c[i+4] + self.c[i+6] + self.c[i+8]+ self.c[i+10])/6
                            #self.c[i+1] =(self.c[i+1] + self.c[i+3] + self.c[i+5] + self.c[i+7]+ self.c[i+9] + self.c[i+11])/6
                            #print(str(self.c[i]) +  "has been set to" + str(self.c[i+400]))
                        #except Exception:
                            #print("ERROR")
                            #pass

                    self.peri = cv2.arcLength(self.c, True)
                    self.approx = cv2.approxPolyDP(self.c, 0.004 * self.peri, True)
                    print ("new length: " + str(len(self.approx)))
                '''

                cv2.circle(self.frame, (int(self.x), int(self.y)), int(self.radius),(0, 255, 255), 2)

                if    len(self.approx) == 3 and self.radius >30:
                    self.shape = "triangle"

                elif    len(self.approx) == 4 and self.radius >30:
                    (x, y, w, h) = cv2.boundingRect(ZEDc.approx)
                    ar = w / float(h)
                    self.shape = "square" if ar >= 0.95 and ar <= 1.05 else "rectangle"

                elif    len(self.approx) == 5 and self.radius >30:
                    self.shape = "pentagon"   

                elif    len(self.approx) == 6 and self.radius >30:
                    self.shape = "hexagon" 

                elif    len(self.approx) == 7 and self.radius >30:
                    self.shape = "septagon"    

                elif    len(self.approx) == 8 and self.radius >30:
                    self.shape = "octagon"

                elif    len(self.approx) == 9 and self.radius >30:
                    self.shape = "ninagon"

                elif    len(self.approx) == 10 and self.radius >30:
                    self.shape = "decagon"

                elif    len(self.approx) > 10 and self.radius >30:
                    self.shape = ("polygon with " + str(len(self.approx)) + " sides")

                elif  len(self.approx) < 3:
                    self.shape = "missingno"

                if    len(self.approx) == edges and 30 < self.radius < 200:
                    print('The object with the right amount of edges was detected!')
                    cv2.drawContours(self.frame, [self.c], -1, (0, 255, 0), 8)
                    self.center = (int(self.M["m10"] / self.M["m00"]), int(self.M["m01"] / self.M["m00"]))
                    cv2.circle(self.frame, self.center, 5, (255, 0, 255), -1)
                    cv2.imshow('found object',self.mask)
                    cv2.imshow('frame',ZEDc.frame)  
                    #print ("The shape detected was a " + str(self.shape))
                    self.coords = self.approx
                    return self.coords
                    return
            else:
                cv2.imshow('frame',ZEDc.frame)       
            self.coords = self.approx
            return self.coords





#This is the main function for this python file
if __name__ == '__main__':
    ZEDc = ZED()
    shape = []

    while True:
        ZEDc.detect_object(edges = 4)
        print ("The shape detected was a " + str(ZEDc.shape))

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    print('ending program')
    #ZEDc.out.release()
    time.sleep(.1)
    ZEDc.ZEDFeed.release()
    cv2.destroyAllWindows()