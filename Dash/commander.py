import rospy
import cv2
from lidar import LIDAR
from zed import ZED
from simplesensorsmap import SimpleSensors
from ackermann_msgs.msg import AckermannDriveStamped
from sensor_msgs.msg import Joy

#The main class for handling all of the sensor data
class Commander:
    def __init__(self):
        
        self.LIDAR = LIDAR()
        self.ZED = ZED()
        
        #This is a placeholder for the physics class
        self.physics = 0.0


        #Initialize the node
        rospy.init_node('Rudolph', anonymous=False)

        rospy.loginfo("To stop: ^C")
        rospy.on_shutdown(self.shutdown)

        # Ackermann drive pub, and zero it
        self.ackerPub = rospy.Publisher('/vesc/ackermann_cmd_mux/input/navigation', AckermannDriveStamped, queue_size=10)

        self.ackermannMsg = AckermannDriveStamped()

        self.acker = self.ackermannMsg.drive
        self.acker.speed = 0
        self.acker.steeringAngle = 0

        self.isAuto = False
        self.isAI = False
        rospy.Subscriber('/vesc/joy', Joy, self.joy_CB)


    #The joystick method
    def joy_CB(self, joy):
        if joy.buttons[0] == 1: # A
            self.isAuto = True

        if joy.buttons[3] == 1: # Y
            self.isAI= True   

        elif joy.buttons[2] == 1: # X
            self.acker.speed = 0
            self.publish_acker()
            self.isAuto = False
            self.isAuto = False

    #The ackermann publishing method
    def publish_acker(self):
        if self.isAuto:
            self.ackerPub.publish(self.ackermannMsg)
        else:
            self.ackerPub.publish(AckermannDriveStamped())


    #Runs the algorithims
    def run_algs(self):

        #Runs the physics algorithim for the lidar
        self.physics = SimpleSensors(initvel = self.acker.speed, inittime =0.0, finaltime = 1, initangle=self.acker.steering_angle, left=self.LIDAR.left, right=self.LIDAR.right, middle=self.LIDAR.middle_view_min)
        self.physics.calculate_lidar_data()
        self.acker.speed = self.physics.speed
        self.acker.steeringAngle = self.physics.steer


    #The start function
    def start(self, rate=20):
        r = rospy.Rate(rate)

        while not rospy.is_shutdown():

            self.run_algs()
            self.publish_acker()

            r.sleep()

    def shutdown(self):
        rospy.loginfo("Stopping Rudolph")
        cv2.destroyAllWindows()

        # sleep makes sure topics recive their 'stop' message fore shutting down bot
        rospy.sleep(1)

if __name__ == "__main__":
    try:
        c = Commander()
        c.start()
    except Exception as e:
        print("Exception: "+ str(e))
