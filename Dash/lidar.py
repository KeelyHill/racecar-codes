import cv2
from sensor_msgs.msg import LaserScan
import numpy as np
import rospy


#The LIDAR class, that will return all of the LIDAR data
class LIDAR:
    def __init__(self):

        #Subscribe to the LIDAR
        rospy.Subscriber('/scan', LaserScan, self.scan_CB)

        #Declare all of the local variables to be used by the master class
        self.scans_len = 0.0
        self.scans_len = 0.0
        self.right = 0.0
        self.left = 0.0
        self.middle_view_min = 0.0
        self.err = 0.0



    #Grabs all of the sensor's data
    def scan_CB(self, scan):

        self.scans_len = len(scan.ranges)
        self.right = scan.ranges[self.scans_len/3]
        self.left = scan.ranges[2*self.scans_len/3]

        self.middle_view_min = np.average(scan.ranges[2*self.scans_len/5 : 3*self.scans_len/5])

        self.err = round(self.right-self.left, 2) / 2
