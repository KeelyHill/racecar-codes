import rospy
from sensor_msgs.msg import Image, LaserScan

import cv2
import numpy as np

MAP_WIDTH = 1000  # cm
MAP_HEIGHT = 1000


class KLocalLaserMap(object):

    def __init__(self):
        rospy.init_node('KLocalLaserMap', anonymous=False)

        rospy.loginfo("To stop: ^C")
        rospy.on_shutdown(self.shutdown)

        self.map_data = np.zeros((MAP_HEIGHT, MAP_WIDTH), np.uint8) # 2d array, 0 black, unknown

        ## Subscribers

        rospy.Subscriber('/scan', LaserScan, self.scan_CB)


    def scan_CB(self, scan):
        # x = r cos theta    y = r sin theta

        buffer_zone = np.zeros((MAP_HEIGHT, MAP_WIDTH), np.uint8) # 2d array, 0 black, unknown

        for i, r in enumerate(scan.ranges): # this assumes moving left in ranges
            theta = scan.angle_min + angle_increment * i
            x = int(r * np.cos(theta) * 100) # * 100 to convert to m > cm
            y = int(r * np.sin(theta) * 100)

            # mark edge buffer, then lidar point of object
            self.map_data[x,y] = 100 # grey, wall/object
            cv2.circle(buffer_zone, (x,y), 10, 150, thickness=-1)

            # mark known empty space
            if x < MAP_WIDTH/2: # left side
                self.map_data[x:MAP_WIDTH,y] = 200
            else:
                for k in range(MAP_WIDTH/2, x): # right side
                    if self.map_data[k,y] == 1:
                        break # stop when hit previous wall
                    self.map_data[k,y] = 200


        # overlay map_data (walls) over buffer_zone
        self.map_data = cv2.addWeighted(self.map_data, 1, buffer_zone, 0, 0)

        # as_img = cv2.cvtColor(self.map_data, cv2.COLOR_GRAY2BGR) # may not be neeeded
        self.imgshow(self.map_data)



    # keep passing data for 'video'
    def imgshow(self, image, named='local laser map'):
        cv2.namedWindow(named, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(named, 960, 540)

        cv2.imshow(named, image)
        cv2.waitKey(1) #needed for some reason, 1 mili sec, shows image and keeps it updating



    def start(self, rate=20):
        r = rospy.Rate(rate)

        while not rospy.is_shutdown():

            r.sleep()


    def shutdown(self):
        rospy.loginfo("Stopping KLocalLaserMap")
        cv2.destroyAllWindows()

        # sleep makses sure topics recive their 'stop' message fore shutting down bot
        rospy.sleep(1)
