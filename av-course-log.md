# AV Course Log

## Build and Pre-code Instructions

### Car build and quick start
1. Build car

2. Flash Tegra

3. Configure Motor Control
   - Load XML [`traxxas_velineon_3500.xml`](https://github.com/mit-racecar/hardware/blob/master/vesc/traxxas_velineon_3500.xml) using the [BLDC tool](https://github.com/vedderb/bldc-tool).
   - Flash a the updated firmware if the steering does not work. The .bin files are in the same repository.

4. Network with Tegra

5. Testing movement with Teleop
    - Raise the car off the group with blocks for first tests.
    - Run: `roslaunch racecar teleop.launch` on the Tegra.
        - should the vesc not be plugged in, an error will occur. Plug it in, then restart the teleop.
    - Using the wireless (USB attached) controller control the car using the analog sticks.
      - Left-upper trigger is a dead man switch for operation.
      - Left analog controls speed, right analog controls steering.
      - The switch on the back of the controller should be on 'X'.
      - If movement does not work, check press "Mode" to switch the left of the controller from digital to analog input and check the correct XML files are loaded onto the motor controller.


### Shutting down the car.

- Run a normal ubuntu shutdown: `sudo shutdown now -h`
- Unplug both power-supplying cables from the computer battery.
- Unplug the motor controller from the car battery

### RVIZ remote topic display

A common task is to SSH into the robot's computer and run RVIZ. Running RVIZ directly on the remote computer will not work due to the way RVIZ is implemented. The workaround is to run RVIZ on a local computer with GUI and connect the local computer to the ROS master of the robot.

Assume:
IP: 10.20.30.11 // remote computer (robot)
IP: 10.20.30.22 // local computer (host)

**ssh into remote computer**
1. `ssh -X racecar@10.20.30.11`

2. `export ROS_MASTER_URI=http://10.20.30.11:11311`   # this ensures that we do not use localhost, but the real IP address as master node

3. `export ROS_IP=10.20.30.11`		# this ensures that ROS knows that we cannot use hostname directly (due to DHCP firewall issues)

4. `roscore`    # or somehow launch master

**At the local terminal:**
1. `export ROS_MASTER_URI=http://10.20.30.11:11311`  # tells local computer to look for the remote here

2. `export ROS_IP=10.20.30.22`	# this ensures that ROS knows that we cannot use hostname directly (due to DHCP firewall issues)

3. `rosrun rviz rviz`			# fires up rviz on local computer. It will attach to the master node of the remote computer


Add the `export xxx` lines to the `~/.bashrc` of the respective machines to run on terminal entrance

([from umd.edu](http://www.umiacs.umd.edu/~cteo/umd-erratic-ros-data/README-rvis-remote))


### Gazebo

#### Setup and first run

This assumes you have already [installed ROS](http://wiki.ros.org/ROS/Installation).

Terminal commands:
```bash
cd ~
git clone https://github.com/mit-racecar/racecar
mv racecar/ racecar-ws

cd racecar-ws/
mv racecar-vm.rosinstall .rosinstall # on laptop or VM, not Tegra

rosinstall .

sudo apt-get install ros-kinetic-ackermann-msgs
sudo apt-get install ros-kinetic-serial

catkin_make

# assuming successful...

echo "source ~/racecar-ws/devel/setup.bash" >> ~/.bashrc
```

That will get all the racecar stuff needed to get it into Gazebo.

To run: `$ roslaunch racecar_gazebo racecar_tunnel.launch`


## Programming examples and discussion

### Basic Ackermann motor and steering control

A basic example publishing an [`AckermannDriveStamped`](http://docs.ros.org/kinetic/api/ackermann_msgs/html/msg/AckermannDriveStamped.html) message to the input navigation topic. Update the [`AckermannDrive`](http://docs.ros.org/kinetic/api/ackermann_msgs/html/msg/AckermannDrive.html) variable `drive`.

```python
# Copyleft 2016 Keely Hill

import rospy

from ackermann_msgs.msg import AckermannDriveStamped

class GoForwardTurning():
    def __init__(self):

        rospy.init_node('GoForwardTurning', anonymous=False)

        rospy.loginfo("To stop: CTRL + C")
        # What function to call when you ctrl + c
        rospy.on_shutdown(self.shutdown)

        # `navigation` is a lower priority than `teleop`, this allows emergency overriding
        self.cmd_vel = rospy.Publisher(
                            '/vesc/ackermann_cmd_mux/input/navigation',
                            AckermannDriveStamped, queue_size=10)

        r = rospy.Rate(10)  # handles 10 times per second

        # change this variable (`move_cmd.drive`) to move and steer
        move_cmd = AckermannDriveStamped()
        move_cmd.drive.speed = 1.5
        move_cmd.drive.steering_angle = 0.5 # radians

        # as long as you haven't ctrl + c keeping doing...
        while not rospy.is_shutdown():

            self.cmd_vel.publish(move_cmd)

            r.sleep()

    def shutdown(self):

        rospy.loginfo("Stoping...")

        self.cmd_vel.publish(AckermannDriveStamped()) # zero all commands

        # sleep just makes sure it receives the stop command prior to shutting down
        rospy.sleep(1)

if __name__ == '__main__':
    try:
        GoForwardTurning()
    except Exception as e:
        print("Exception: "+ str(e))
        rospy.loginfo("GoForwardTurning node terminated.")

```

To run, first: `roslaunch racecar teleop.launch`; this starts the core and initiates sensors and topics.
Then simply run the python script as another process.

#### Adding subscribers to sensors

Every topic subscription calls a given callback in realtime with the message as the argument. The bellow can be added to the GoForwardTurning class example, or work in any class. Determine the message type with `rostopic info /the/topic/path` or `rqt_graph` or any means. Search the ROS docs for the message type to learn how it gives data.

```python
from sensor_msgs.msg import LaserScan # for use in subscribing

# ... somewhere in __init__:

# pass: topic, type (class) of message from import, and any callback.
rospy.Subscriber('/scan', LaserScan, self.scan_CB)

# ... somewhere in class body:

def scan_CB(self, scan):
    # get middle range, for example
    scan.ranges[len(scan.ranges)/2]

```


### UInt8 image data to CV image

Getting ZED publishing: `roslaunch zed_wrapper`

```python
import cv_bridge

self.bridge = cv_bridge.CvBridge()

def zed_depth_image_CB(self, imgmsg):
    image = self.bridge.imgmsg_to_cv2(imgmsg, desired_encoding='bgr8')

```

```python
# !!! this does not work with kinetic !!!

import cv2
import numpy as np

# callback of subscriber
def zed_compressed_CB(self, img_msgs):
    img = cv2.imdecode(np.fromstring(img_msgs.data, np.uint8), cv2.CV_LOAD_IMAGE_COLOR)
    # that's it
```

Depth image to 'distance pixels'
```python
def zed_depth_image_CB(self, imgmsg):

    dmap = np.fromstring(imgmsg.data, dtype='<f4')

    # optionally clean data
    dmap[dmap >= 7 ] = 0
    dmap[dmap < 7 ] = 255
    dmap[np.isnan(dmap)] = 0

    dmap.shape = (imgmsg.height, imgmsg.width)

    # print(dmap)
    # self.imgshow(dmap)
```

### Wall centering

One approach to staying centered between to walls is sampling the sides of the LIDAR range data. The data of each side is compared to find and error, which is used to steer to lessen the error.
My (Keely) first approach was average a third on each side. However, the large range of data points caused the car to sway in the center. By sampling a single point on each side, centering was smooth and consistent. The disadvantage to this the usage of less data. It works fine when the course is known to have consistent walls and no weird occurrences. Keep in mind: both of these method have no forward collision avoidance.

### Mounting SD Card

`sudo mount -o rw /dev/mmcblk1p1 /media/sdcard/` (temp working out non-hacky method)

### Mapping and localizing

Basic mapping is a follows. It may vary depending on the package used, but the concept stays the same. You can view the map topic update live (using the remote topic rviz show above).

1. With the master (e.g. teleop) started, `rosrun gmapping slam_gmapping`.

2. Move the robot around. With some experimentation, we determined that:
 - Covering the same area from multiple paths is beneficial
 - Slowly swearing helps get more detail and cover more wall area.
 - Quickly turning back and forth is detrimental and could lead to map overright at another angle. (Issue if IMU not used, relaying on motor controller)
 - If an IMU is not used, moving the robot other than by some control, will cause mapping error.

3. Save the map using map_server while gmapping is still running:  
  `rosrun map_server map_saver -f mymap`
 - It will create two files: `mymap.pgm`, and `mymap.yaml`; a bitmap and small map metadata file.
 - The bitmap can be edited by image manipulation software for basic cleanup.

4. Serve the map again (to the `/map` topic): `rosrun map_server map_server mymap.yaml`.

5. Now ready to localize.




Convert between Quaternion and Yaw (known that Quaternion is 2D, maps to yaw):

```python
from tf.transformations import quaternion_from_euler, euler_from_quaternion

# From quaternion
row, pitch, yaw = euler_from_quaternion((x,y,z,w)) # note: as tuple

# To quaternion
quaternion = quaternion_from_euler(0, 0, yaw_angle) # roll, pitch, yaw
#type(pose) = geometry_msgs.msg.Pose
pose.orientation.x = quaternion[0]
pose.orientation.y = quaternion[1]
pose.orientation.z = quaternion[2]
pose.orientation.w = quaternion[3]
```


Ackermann steering angle <-> Ackermann turning radius
```
turnR = wheelbase/tan(angle)
angle = tan^-1(wheelbase/r)
```


## Readings

### Route planning

- [Kinematic Path Planning of Non-Holonomic Any-Shape Vehicles](http://journals.sagepub.com/doi/pdf/10.5772/60463)
- [Autonomous navigation framework for a car-like robot](http://www.iri.upc.edu/files/scidoc/1658-Autonomous-navigation-framework-for-a-car-like-robot.pdf)


## Workflow
A high level description of Keely's development on with the car.

Outside of class (sometimes on non-busy morning), after conceptualizing a task, I write the software for it -- expecting errors. (Because the car is not internet connected to do a git pull) I then use `scp` to copy the file or directory to the car, and run it in an ssh window (using `screen`). I then fix the small errors, the move to diagnosing logical issues with the code. Depending on the comnplexity of the changes, I edit via `vim` on the car, or on an editor on my laptop to the `scp` the changes back over.


## Final exam techniques and thoughts
The backbone of the final exam is teb local planner. Intermediate goals were set along the track. The launch files, configuration, and a commanding multiplexer were all created in the proper ROS package [`rudolph`](/rudolph).

However, for the final demo, we went just with an improved stay middle (`stay_middle_8.py`) with stop sign functionality. TEB had problems on the tight course and limited computing power with all the things running.

This has been a fun project.
